<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 18.04.18
 * Time: 21:14
 *
 * TODO: add in app as route before init and make a view in views foundation dir
 */

class Management_controller
{
    public $name;

    function __construct($name)
    {
        $this->name = $name;
        if ($this->name == '')
        {
            $this->name = 'home';
        };
    }

    function execute($action = '', $parameter = '')
    {
        define('CONTROLLER_NAME', $this->name);
        define('CONTROLLER_ACTION', $action);
        define('CONTROLLER_PARAMETER', $parameter);

        $viewname = $this->name;

        $file_name = APP_FOUNDATION_SERVER_PATH.'/views/'.$viewname.'.php';

        if (file_exists($file_name))
        {
            return $file_name;
        }

        return null;
    }

}