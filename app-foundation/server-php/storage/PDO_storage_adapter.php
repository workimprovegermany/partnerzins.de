<?php

class PDO_storage_adapter extends Storage_adapter
{
    public $db_con = null;
    private $config_file;

    public function __construct($config_file)
    {
        $this->config_file = $config_file;
        if ($this->db_con == null) $this->open_db_connection();
    }

    /**
     * reads config_file and creates PDO object
     */
    function open_db_connection()
    {
        $config = new Config($this->config_file);
        $dbc = $config->db();
        $this->db_con = new PDO('mysql:host='.$dbc['host'].';dbname='.$dbc['db_name'], $dbc['db_user'], $dbc['db_pass']);
    }

    public function find($id, $table)
    {
        $statement = $this->db_con->prepare("SELECT * FROM $table WHERE id = :id LIMIT 1");
        $statement->execute(array('id' => $id));
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function find_by_fields($fields, $table, $limit = null, $offset = null, $sort_key = null, $sort_order = null)
    {
        $ws = $this->build_where_clause_from_array($fields);

        $query = "SELECT * FROM $table WHERE $ws";
        if ($sort_key != null) {
            $query .= " ORDER by `$sort_key`";
            if ($sort_order != null)
            {
                $query .= ' DESC';
            }
        }
        if ($limit != null) $query .= " LIMIT $limit";
        if ($offset != null) $query .= " OFFSET $offset";

        $statement = $this->db_con->prepare($query);
        $statement->execute($this->build_bind_values_from_array($fields));

        $result = array();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC))
        {
            array_push($result, $row);
        }

        if (empty($result)) $result = null;

        return $result;
    }

    public function find_ids_by_fields($fields, $table, $limit = null, $offset = null, $sort_key = null, $sort_order = null)
    {
        $ws = $this->build_where_clause_from_array($fields);

        $query = "SELECT id FROM $table WHERE $ws";

        if ($sort_key != null)
        {
            $query .= " ORDER by `$sort_key`";
            if ($sort_order != null)
            {
                $query .= ' DESC';
            }
        }
        if ($offset != null) $query .= " OFFSET $offset";
        if ($limit != null) $query .= " LIMIT $limit";

        $statement = $this->db_con->prepare($query);
        $statement->execute($this->build_bind_values_from_array($fields));

        $result = array();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC))
        {
            array_push($result, $row['id']);
        }
        return $result;
    }

    public function count_by_fields($fields, $table)
    {
        $ws = $this->build_where_clause_from_array($fields);

        $statement = $this->db_con->prepare("SELECT COUNT(*) FROM $table WHERE $ws");
        $statement->execute($this->build_bind_values_from_array($fields));
        $row = $statement->fetchColumn();
        return $row;
    }

    public function sum_column_by_fields($column, $fields, $table)
    {
        $ws = $this->build_where_clause_from_array($fields);

        $statement = $this->db_con->prepare("SELECT SUM($column) FROM $table WHERE $ws");
        $statement->execute($this->build_bind_values_from_array($fields));
        $row = $statement->fetchColumn();
        return $row;
    }













    /**
     * @param string $field
     * @param string $value
     * @param string $table
     *
     * @return array|null
     */
    public function find_by_field($field, $value, $table)
    {
        $statement = $this->db_con->prepare("SELECT id FROM $table WHERE $field = :value");
        $statement->execute(array("value" => $value));

        $result = array();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC))
        {
            array_push($result, $row['id']);
        }
        return $result;
    }

    public function id_for_field($field, $table)
    {
        $bed = array();
        foreach ($field as $k => $v)
        {
            array_push($bed, "$k = :$k");
        }
        $ws = implode(" AND ",$bed);


        $statement = $this->db_con->prepare("SELECT id FROM $table WHERE $ws LIMIT 1");
        $statement->execute($field);
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        return $row['id'];
    }

    public function rows_for_field($field, $table)
    {
        $bed = array();
        foreach ($field as $k => $v)
        {
            array_push($bed, "$k = :$k");
        }
        $ws = implode(" AND ",$bed);


        $statement = $this->db_con->prepare("SELECT * FROM $table WHERE $ws");
        $statement->execute($field);

        $result = array();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC))
        {
            array_push($result, $row);
        }
        return $result;
    }

    public function distinct_values_for_column($table, $column)
    {
        $col_str = strval($column);
        $dv = array();
        $statement = $this->db_con->prepare("SELECT DISTINCT $col_str FROM $table ORDER BY $col_str");
        $statement->execute();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC))
        {
            if (!empty($row[$col_str]))
            {
                array_push($dv, $row[$col_str]);
            }
        }
        return $dv;
    }



    /**
     * @param array $field_array
     * @param string $table
     * @return mixed
     */
    public function insert($field_array, $table)
    {
        $query = "INSERT INTO $table (".implode(",", array_keys($field_array)).") 
                  VALUES ( :".implode(", :",array_keys($field_array)).")";
        $statement = $this->db_con->prepare($query);
        foreach($field_array as $key => $value)
        {
            $statement->bindValue(':'.$key, $value);
        }
        $result = $statement->execute();

        if(!$result) {
            echo 'Beim Abspeichern ist leider ein Fehler aufgetreten<br>';
            echo "SQL Error <br />";
            echo $statement->queryString."<br />";
            echo $statement->errorInfo()[2];

            return $result;
        }

        $statement = $this->db_con->prepare("SELECT LAST_INSERT_ID() as last_id");
        $result = $statement->execute();
        $result = $statement->fetch();

        return $result;
    }

    /**
     * @param string $table
     * @param int $id
     * @param array $fields
     * @return mixed
     */
    public function update($table, $id, $fields)
    {
        $keylist = array();
        foreach ($fields as $key => $value)
        {
            $str = $key." = :".$key;
            array_push($keylist, $str);
        }

        $query = "UPDATE $table SET ".implode(", ",$keylist)." WHERE id = :id";

        //error_log(print_r($attr, TRUE));
        error_log($query);


        $statement = $this->db_con->prepare($query);
        foreach($fields as $key => $value)
        {
            !empty($value) ? $statement->bindValue(':'.$key, $value) : $statement->bindValue(':'.$key, NULL);
        }
        $statement->bindValue(':id', $id);
        $result = $statement->execute();

        if(!$result) {
            echo 'Beim Updaten ist leider ein Fehler aufgetreten<br>';
            echo "SQL Error <br />";
            echo $statement->queryString."<br />";
            echo $statement->errorInfo()[2];
        }

        return $result;

    }

    /**
     * @param int $id
     * @param string $table
     * @return mixed
     */
    public function delete($id, $table)
    {
        $query = "DELETE FROM $table WHERE id = :id";
        $statement = $this->db_con->prepare($query);
        $statement->bindValue(':id', $id);
        $result = $statement->execute();

        if(!$result) {
            echo 'Beim Löschen ist leider ein Fehler aufgetreten<br>';
            echo "SQL Error <br />";
            echo $statement->queryString."<br />";
            echo $statement->errorInfo()[2];
        }

        return $result;
    }

    /**
     * Returns field from a table for a row (ID).
     *
     * @param string $table
     * @param string $id
     * @param string $field
     *
     * @return string|null
     */
    public function get_field($table, $id, $field)
    {
        $statement = $this->db_con->prepare("SELECT $field FROM $table WHERE `id` = :value LIMIT 1");
        $statement->execute(array("value" => $id));

        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (!isset($result[$field]))
        {
            return false;
        }

        return $result[$field];
    }

    /**
     * @param string $query
     *
     * @return array
     */
    public function query_all($query)
    {
        $ret = array();

        foreach ($this->db_con->query($query) as $row)
        {
            array_push($ret, $row);
        };

        return $ret;
    }

    /**
     *  The following functions are for compatibility issues with the old mysql interface.
     *  So the old code can easily be changed to PDO.
     *
     *  Just add to the global_funcs.php (where $db_sets is declared):
     *
     *  require_once "../php-foundation/foundation.php";
     *  $pdo = new PDO_storage_adapter('../cfg_imparare/config.php');
     *
     *  And replace $db_sets with $pdo
     *  old code:
     *  $d = $db_sets->query(...)
     *  while ($get = $db_sets->fetch_array($d))
     *
     *  new code:
     *  $d = $pdo->query(...)
     *  while ($get = $pdo->fetch_array($d))
     *
     */


    /**
     *
     * @param string $query
     *
     * @return PDO::Statement
     */
    public function query($query)
    {
        $statement = $this->db_con->prepare($query);
        $statement->execute();

        return $statement;
    }

    /**
     *
     * @param PDO::Statement $statement
     *
     * @return array
     */
    public function fetch_array($statement)
    {
        $ret = $statement->fetch(PDO::FETCH_ASSOC);
        return $ret;
    }

    /**
     *
     * @param PDO::Statement $statement
     *
     * @return int
     */
    public function num_rows($statement)
    {
        $ret = $statement->rowCount();
        return $ret;
    }

    /**
     * Finds first row that has the $field and matching $value.
     * Returns ID as string.
     *
     * @param string $table
     * @param string $field
     * @param string $value
     *
     * @return string|false
     */
    public function find_unique_by_field($table, $field, $value)
    {
        $statement = $this->db_con->prepare("SELECT id FROM $table WHERE $field = :value LIMIT 1");
        $statement->execute(array("value" => $value));

        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (!isset($result['id']))
        {
            return false;
        }

        return $result['id'];
    }












    private function build_where_clause_from_array($fields)
    {
        //TODO: empty fields
        //TODO: use Filter class over arrays to handle special cases (e.g. LIKE)
        $bed = array();
        foreach ($fields as $k => $v)
        {
            $op = '=';

            if (is_array($v))
            {
                $parts = array();
                $i = 1;
                foreach ($v as $val)
                {
                    $bind_key = $k.$i++;
                    array_push($parts, "$k $op :$bind_key");
                }
                array_push($bed, '('.implode(" OR ",$parts).')');
            }
            else
            {
                if (substr($v, 0, strlen('LIKE ')) === 'LIKE ')
                {
                    $op =  'LIKE';
                    //$fields[$k] = substr($v, strlen('LIKE ')).'%';
                }

                if (substr($v, 0, strlen('>')) === '>')
                {
                    $op =  '>';
                    //$fields[$k] = substr($v, strlen('>'));
                }

                if (substr($v, 0, strlen('>=')) === '>=')
                {
                    $op =  '>=';
                    //$fields[$k] = substr($v, strlen('>='));
                }

                array_push($bed, "$k $op :$k");
            }

        }
        $ws = implode(" AND ",$bed);

        return $ws;
    }

    private function build_bind_values_from_array($fields)
    {
        //TODO: empty fields
        //TODO: use Filter class over arrays to handle special cases (e.g. LIKE)
        $bind = array();
        foreach ($fields as $k => $v)
        {
            if (is_array($v))
            {
                $i = 1;
                foreach ($v as $val)
                {
                    $bind_key = $k.$i++;
                    $bind[$bind_key] = $val;
                }
            }
            else
            {
                if (substr($v, 0, strlen('LIKE ')) === 'LIKE ')
                {
                    $bind[$k] = substr($v, strlen('LIKE ')).'%';
                }
                elseif (substr($v, 0, strlen('>')) === '>')
                {
                    $bind[$k] = substr($v, strlen('>'));
                }
                elseif (substr($v, 0, strlen('>=')) === '>=')
                {
                    $bind[$k] = substr($v, strlen('>='));
                }
                else
                {
                    $bind[$k] = $v;
                }

            }

        }

        return $bind;
    }
}
