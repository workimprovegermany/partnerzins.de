<?php

class Filter
{
    public $name;
    public $data;
    public $value_type;
    public $component_type;

    public $value;
    public $has_value = false;

    public function __construct($name, $app, $value_type, $component_type)
    {
        $this->name = $name;
        $this->data = $app->pdo->distinct_values_for_column('products', $name);
        $this->value_type = $value_type;
        $this->component_type = $component_type;
    }

    public function set($filter_value)
    {
        if ($this->value_type == 'bool')
        {
            if ($filter_value == $this->name)
            {
                $this->value = true;
                $this->has_value = true;
            }
        }
        elseif ($this->value_type == 'absolute')
        {
            if (in_array($filter_value,$this->data))
            {
                $this->value = $filter_value;
                $this->has_value = true;
            }
        }
        elseif ($this->value_type == 'range')
        {
            $delimiter_found = strpos($filter_value, '-');
            $pricerange = explode('-',$filter_value);

            if ($delimiter_found && is_numeric($pricerange[0]) && is_numeric($pricerange[1]))
            {
                $this->value = array($pricerange[0], $pricerange[1]);
                $this->has_value = true;
            }
        }
    }

    public function get_formatted_value()
    {
        if (!$this->has_value) return false;

        if ($this->value_type == 'bool')
        {
            // TODO: read from description of field
            return 'Sale';
        }
        elseif ($this->value_type == 'absolute')
        {
            return $this->value;
        }
        elseif ($this->value_type == 'range')
        {
            return $this->value[0] . "€ - " . $this->value[1] . "€";
        }
    }

    public function get_unformatted_value()
    {
        if (!$this->has_value) return false;

        if ($this->value_type == 'bool')
        {
            // TODO: read from description of field
            return 'Sale';
        }
        elseif ($this->value_type == 'absolute')
        {
            return $this->value;
        }
        elseif ($this->value_type == 'range')
        {
            return $this->value[0] . "-" . $this->value[1];
        }
    }

    public function get_filter_box($url_builder, $app)
    {
        if ($this->value_type == 'bool')
        {
            return $this->get_bool_filter_box($url_builder, $app);
        }
        elseif ($this->value_type == 'absolute')
        {
            return $this->get_absolute_filter_box($url_builder, $app);
        }
        elseif ($this->value_type == 'range')
        {
            return $this->get_range_filter_box($url_builder, $app);
        }
    }

    public function get_bool_filter_box($url_builder, $app)
    {
        $oldhas = $this->has_value;
        $oldvalue = $this->value;


        $filter_box = "<div class=\"well\" style=\"min-height: 250px;\">";
        $this->value = true;
        $this->has_value = true;
        $link = $url_builder->get_url_for_filters($app->filter);
        $filter_box .= "<a href=\"$link\">nur Sale<br/></a>";
        $filter_box .= "</div>";

        $this->value = $oldvalue;
        $this->has_value = $oldhas;
        return $filter_box;
    }

    public function get_absolute_filter_box($url_builder, $app)
    {
        $filter_box = file_get_contents("../views/templates/filter_box_absolute.tpl");
        $config_xml = new SimpleXMLElement(file_get_contents("../models/product/1.0/config.xml"));
        $desc = $config_xml->xpath('/config/fields/field[name="'.$this->name.'"]/description')[0];
        $filter_box = str_replace('{{description}}', $desc, $filter_box);

        $replacement = "";
        $oldhas = $this->has_value;
        $oldvalue = $this->value;

        if ($this->value_type == 'bool')
        {
            $filter_box = "<div class=\"well\" style=\"min-height: 250px;\">";
            $this->value = true;
            $this->has_value = true;
            $link = $url_builder->get_url_for_filters($app->filter);
            $filter_box .= "<a href=\"$link\">nur Sale<br/></a>";
            $filter_box .= "</div>";
        }
        else
        {
            foreach($this->data as $item)
            {
                $selected = $oldhas && ($oldvalue == $item);
                $this->value = $item;
                $this->has_value = true;
                $link = $url_builder->get_url_for_filters($app->filter);
                $replacement .= "<a href=\"$link\"";
                if ($selected)
                {
                    $replacement .= " style=\"color: #000;\"";
                }
                $replacement .= ">$item<br/></a>";
            }
            $filter_box = str_replace('{{list}}', $replacement, $filter_box);
        }
        $this->value = $oldvalue;
        $this->has_value = $oldhas;
        return $filter_box;
    }

    public function get_range_filter_box($url_builder, $app)
    {
        $config_xml = new SimpleXMLElement(file_get_contents("../models/product/1.0/config.xml"));
        echo "<div class=\"well\"  style=\"min-height: 250px;\">";
        $desc = $config_xml->xpath('/config/fields/field[name="'.$this->name.'"]/description')[0];
        echo "<h5>$desc</h5>";
        echo "<br/>";

        echo "<div class=\"input-group\" style=\"width: 100%\">";
        //echo "<b style=\"margin-right: 10px;\">€ 0</b>";
        echo "<input id=\"InputPreis1\" name=\"price\" type=\"text\" class=\"span2\" value=\"\" data-slider-min=\"0\" data-slider-max=\"500\" data-slider-step=\"5\"";

        $pf = $this->has_value ? $this->value[0] : 0;
        $pt = $this->has_value ? $this->value[1] : 500;
        echo " data-slider-value=\"[$pf,$pt]\"";
        echo " width=\"100%\"/>";
        //echo "<b style=\"margin-left: 10px;\"> € 500</b>";
        echo "</div>";

        echo "<div class=\"input-group\">";
        echo "<span class=\"input-group-addon\" id=\"basic-addon1\">€</span>";
        echo "<input id=\"InputPreis2\" onchange=\"price_input_changed()\" type=\"number\" class=\"form-control\" placeholder=\"Preis von\" aria-describedby=\"basic-addon1\"";
        if ($this->has_value)
        {
            $pf = $this->value[0];
            echo " value=\"$pf\"";
        }
        echo ">";
        echo "</div>";

        echo "<div class=\"input-group\">";
        echo "<span class=\"input-group-addon\" id=\"basic-addon2\">€</span>";
        echo "<input id=\"InputPreis3\" onchange=\"price_input_changed()\" type=\"number\" class=\"form-control\" placeholder=\"Preis bis\" aria-describedby=\"basic-addon2\"";
        if ($this->has_value)
        {
            $pt = $this->value[1];
            echo " value=\"$pt\"";
        }
        echo ">";
        echo "</div>";

        echo "<a id=\"filter_button\" class=\"btn btn-default\" role=\"button\">Filtern</a>";
        echo "</div>";
    }




    // static helper functions

    public static function get_filter_by_name($name, $filter_array)
    {
        foreach ($filter_array as $filter)
        {
            if ($filter->name == $name)
            {
                return $filter;
            }
        }
        return false;
    }

    public static function get_filters_without_name($name, $filter_array)
    {
        $ret = array();
        foreach ($filter_array as $filter)
        {
            if ($filter->name != $name)
            {
                array_push($ret, $filter);
            }
        }
        return $ret;
    }

    public static function make_filter_array($filter_array)
    {
        $filters = array();
        foreach ($filter_array as $f)
        {
            $filters[$f->name] = $f->value;
        }
    }

    public static function echo_all_filters($filter_array)
    {
        foreach ($filter_array as $f)
        {
            echo "Filter: " . $f->name;
            if (!empty($f->value))
            {
                echo " value: ";
                var_dump($f->value);
            }
            echo "<br/>";
        }
    }

    public static function get_filters_with_value($filter_array)
    {
        $ret = array();
        foreach ($filter_array as $filter)
        {
            if ($filter->has_value)
            {
                array_push($ret, $filter);
            }
        }
        return $ret;
    }

    public static function create_filter_array_from_config($config_path, $app)
    {
        $config_xml = new SimpleXMLElement(file_get_contents("../models/product/1.0/config.xml"));
        $fields = $config_xml->xpath('/config/fields/field[filter]');

        $ret = array();
        foreach ($fields as $field)
        {
            $name = strval($field->name);
            $value_type = $field->xpath("./filter/value_type")[0]->__toString();
            $component_type = $field->xpath("./filter/component_type")[0]->__toString();
            $f = new Filter($name, $app, $value_type, $component_type);
            array_push($ret, $f);
        }

        return $ret;
    }

    public static function set_filtervalues_from_url($filter_array)
    {
        $path = '';
        $url_comp_arr = parse_url($_SERVER['REQUEST_URI']);
        if (isset($url_comp_arr['path']))
        {
            $path = trim($url_comp_arr['path'],'/');
        }

        $path_arr = explode('/', $path);
        $path_filter_entity = array_shift($path_arr);
        // Entity always 'rucksaecke'
        $path_filter = array_shift($path_arr);

        //2. Filter exploden und trimmen
        $array_values = explode("-", $path_filter);
        $trimmed_array_values = array_map('trim', $array_values);

        foreach ($trimmed_array_values as $filter_value)
        {
            $fv = rawurldecode($filter_value);
            // 3. Unterstrich in Bindestrich
            $fv = str_replace("_", "-", $fv);
            // 3.1 %20 in Leerzeichen
            $fv = str_replace("%20", " ", $fv);

            //jede Komponente mit Datenbankfeldern vergleichen und bei Treffer in array speichern

            foreach ($filter_array as $filter)
            {
                $filter->set($fv);
            }
        }

        //Preisparameter
        if (isset($_GET['amazon_offer_price']))
        {
            foreach ($filter_array as $filter)
            {
                if ($filter->name == 'amazon_offer_price')
                {
                    $filter->set($_GET['amazon_offer_price']);
                }
            }
        }
    }

    public static function get_js_for_filters($filter_array)
    {
        $ret = "<script>" . file_get_contents("../public/js/slider.js") . "</script>";
        return $ret;
    }
}