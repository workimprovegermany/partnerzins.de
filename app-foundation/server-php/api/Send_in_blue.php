<?php

/**
 * Created by PhpStorm.
 * User: marco
 * Date: 27.07.17
 * Time: 17:10
 */

require (APP_FOUNDATION_SERVER_PATH . '/../vendor/mailin-api-v2.0/Mailin.php');


class Send_in_blue
{
    private $sib_base_url;
    private $sib_api_key;
    private $sib_timeout;
    private $sib_list_id;

    public function __construct($config)
    {
        //TODO: test if $config contains valid values
        $this->sib_base_url = $config['sib_base_url'];
        $this->sib_api_key = $config['sib_api_key'];
        $this->sib_timeout = $config['sib_timeout'];
        $this->sib_list_id = $config['sib_list_id'];
    }

    public function set_list_id($id)
    {
        $this->sib_list_id = $id;
    }

    public function create_update_user($teilnehmer, $export_config)
    {
        $export_fields = array(
            "vorname" => "Vorname",
            "name" => "Name",
            "email" => "E-Mail",
            "anrede" => "Anrede",
            "ansprache" => "Ansprache",
            "videotitel" => "Videotitel",
            "videountertitel" => "Videountertitel",
            "videoid" => "Webinar ID",
            "videodatum" => "Datum Teilnahme",
            "teilgenommen" => "teilgenommen",
            "strasse" => "Straße",
            "plz" => "PLZ",
            "ort" => "Ort",
        );

        // sollen in db stehen
        $imparare_field_names = array(
            "vorname" => "vorname",
            "name" => "name",
            "email" => "email",
            "anrede" => "anrede",
            "ansprache" => "ansprache",
            "videotitel" => "video_title",
            "videountertitel" => "video_untertitel",
            "videoid" => "video_id",
            "videodatum" => "teilgenommen_datum",
            "teilgenommen" => "teilgenommen",
            "strasse" => "strasse",
            "plz" => "plz",
            "ort" => "ort",
        );

        $required_export_fields = array('vorname', 'name', 'email');

        $export = array();
        foreach ($export_fields as $field => $descr)
        {
            // wenn Pflichtfeld oder Exporthaken gesetzt ist
            if (in_array($field, $required_export_fields) || $export_config[api_table_export_column_name($field)] == 1)
            {
                // übernehmen bedeutet im array zu setzen: mailer_feldname => wert_von(imparare_feldname)
                $export[$export_config[api_table_mailer_column_name($field)]] = $teilnehmer->nice_field($imparare_field_names[$field]);
            }
        }


        $mailin = new Mailin($this->sib_base_url, $this->sib_api_key, $this->sib_timeout);    //Optional parameter: Timeout in MS

        /*$data = array(
            "email" => $teilnehmer->field('email'),
            "attributes" => array(
                "NAME"=>$teilnehmer->field('name'),
                "VORNAME"=>$teilnehmer->field('vorname'),
                "TELEFON"=>$teilnehmer->field('telefon'),
                "ANREDE"=>$teilnehmer->field('anrede') == 1 ? 'Herr' : 'Frau',
                "ANSPRACHE"=>$teilnehmer->field('ansprache'),
                "IP"=>$teilnehmer->field('teilgenommen_ip'),
                "SESSION"=>$teilnehmer->field('teilgenommen_session'),
                "ZUORDNUNG"=>strtoupper($teilnehmer->field('zuordnung')),
                "FIRMA"=>$teilnehmer->field('firma'),
                "VONSEITE"=>$teilnehmer->field('video_id'),
                //"SMS"=>' ',
            ),
        );*/
        $data = array(
            "email" => $teilnehmer->field('email'),
            "attributes" => $export,
            "listid" => $this->sib_list_id,
        );

        print_r($data);

        $ret = $mailin->create_update_user($data);

        if ($ret["code"]== "success")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function api_table_export_column_name($fieldname)
{
    return "p_".$fieldname."_export";
}
function api_table_imparare_column_name($fieldname)
{
    return "p_".$fieldname."_imparare";
}
function api_table_mailer_column_name($fieldname)
{
    return "p_".$fieldname."_mailer";
}