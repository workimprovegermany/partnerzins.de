<?php
require_once (APP_ROOT_PATH."/vendor/slim.php");

class User_file_upload_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('user.file_upload');
    }

    public function execute()
    {
        $images = Slim::getImages('user_bild');
        $image = array_shift($images); //only one image



        // if we've received output data save as file
        if (isset($image['output']['data'])) {

            $name = $image['output']['name'];
            $data_output = $image['output']['data'];
            $name2 = str_replace(' ', '', $name);

            $target_path = APP_ROOT_PATH . '/_media/'.$this->data['entity_folder'].'/'.$this->data['entity_id'] . '/';

            Slim::saveFile($data_output, $name2, $target_path, false);

            /*// Move file to ftp
            $target_file = $target_path . $name2;
            $local_file = $target_file;
            $remote_path = $this->data['entity_folder']."/".$this->data['entity_id'] . "/";
            $remote_file = $name2;

            $ftp_settings = $this->app->config()->ftp_settings($this->data['config']);
            $file_path_datenbank = $ftp_settings['pfad'] . '/' . $this->data['entity_folder'] . '/' . $this->data['entity_id'] . '/';

            // set up basic connection
            $ftp_conn = ftp_connect($ftp_settings['host']);

            $ftp_login = ftp_login($ftp_conn, $ftp_settings['user'], $ftp_settings['pass']);

            if (ftp_nlist($ftp_conn, $remote_path) == false) {
                ftp_mkdir($ftp_conn, $remote_path);
            }
            ftp_chdir($ftp_conn, $remote_path);

            ftp_put($ftp_conn, $remote_file, $local_file, FTP_BINARY);
            ftp_close($ftp_conn);
            */
            // delete tmp-file
            // unlink($target_file);
            $doc_settings = $this->app->config()->doc_settings($this->data['config']);

            $file_path_datenbank = $doc_settings['url'] . '/_media/' . $this->data['entity_folder'] . '/' . $this->data['entity_id'] . '/';
            $app = App::get_instance();

            // 1. config_path für entity_name besorgen (steht in app_config)
            $app_config = $app->config();
            $entity_config = $app_config->entity_config_for_name($this->data['entity_name']);

            // 2. Entity_mapper initialisieren
            $em = new Entity_mapper($app->db(), $entity_config);

            // 3. Entity mit der übermittelten id (this data id) aus db laden ($em->find_by_fields)
            $entity = $em->find_by_id($this->data['entity_id']);
            // 4. set entity Daten
            $entity->set_field($this->data['entity_fields']['1'], $file_path_datenbank);
            $entity->set_field($this->data['entity_fields']['2'],$name2);

            // 5. Entity über den mapper ($em) wieder speichern
            $em->save($entity);

            $event = new Event();
            $event->set_name('entity.saved')->set_data($this->data['entity_name']);

            header('Content-type: application/json');
            echo $event->to_json();

        }

    }



}