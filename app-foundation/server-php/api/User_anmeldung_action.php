<?php


class User_anmeldung_action extends AF\Action implements Event_notification_interface
{
    public static function get_events_to_notify()
    {
        return array('user.anmeldung');
    }

    public function execute()
    {
        $user_name = $_POST['email_user'];
        $partner_id = $_POST['id_partner'];

        $em = new Entity_mapper($this->app->db(),'user_preaccount/1.0/config.xml');
        $fieldsAcc = array(
            'uname' => $user_name,
        );
        $user_account_array = $em->find_by_fields($fieldsAcc);
        if($user_account_array == null) {
            // Daten aus tbl skynet_affiliates
            $skynet = new Entity_mapper($this->app->db(),'user/1.0/config.xml');
            $fieldSkynet = array(
                'affiliate_short_id'=> $partner_id,
            );
            $entity_skynet = $skynet->find_by_fields($fieldSkynet); // (A)

           // echo $entity_skynet[0];
            $e = Entity_config::create('user_preaccount/1.0/config.xml');
            $e->set_field('uname',$user_name);
            $e->set_field('session',session_id());
            $e->set_field('affiliate_short_id',$partner_id);
            $e->set_field('affiliate_uid',$entity_skynet[0]->field('affiliate_uid'));
            $em->insert($e);


            $user_account = $em->find_by_fields($fieldsAcc,1)[0];

            //echo $user_account->id();

            $mail_event = new Event('mail.send', [
                'config' => 'supportmails',
                'template' => ['mails/partneraccount_email_bestaetigen.html',
                    [
                        'user_preaccount' => $user_account->id(),
                    ]
                ],
            ]);
            $this->app->dispatch_event($mail_event);

            $event = new Event();
            $event->set_name('anmeldung.erfolgreich')->set_data($user_account->field('session'));

        }
        else {
            $event = new Event();
            $event->set_name('anmeldung.error')->set_data('error');
        }


        // print_r($user_account->id());

             $this->app->dispatch_event($event);
             header('Content-type: application/json');
             echo $event->to_json();

            }
      //  }
   // }
}
