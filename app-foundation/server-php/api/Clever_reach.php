<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 08.03.18
 * Time: 10:13
 */

class Clever_reach
{
    private $base_url;
    private $api_id;
    private $api_username;
    private $api_password;

    private $list_id;

    public function __construct($config)
    {
        //TODO: test if $config contains valid values
        $this->base_url = 'https://rest.cleverreach.com/v2';
        $this->api_id = $config['api_id'];
        $this->api_username = $config['api_username'];
        $this->api_password = $config['api_password'];

        $this->list_id = $config['list_id'];
    }

    public function set_list_id($id)
    {
        $this->list_id = $id;
    }

    public function create_update_user($teilnehmer, $export_config)
    {
        $export_fields = array(
            "vorname" => "Vorname",
            "name" => "Name",
            "email" => "E-Mail",
            "anrede" => "Anrede",
            "ansprache" => "Ansprache",
            "videotitel" => "Videotitel",
            "videountertitel" => "Videountertitel",
            "videoid" => "Webinar ID",
            "videodatum" => "Datum Teilnahme",
            "teilgenommen" => "teilgenommen",
            "strasse" => "Straße",
            "plz" => "PLZ",
            "ort" => "Ort",
        );

        // sollen in db stehen
        $imparare_field_names = array(
            "vorname" => "vorname",
            "name" => "name",
            "email" => "email",
            "anrede" => "anrede",
            "ansprache" => "ansprache",
            "videotitel" => "video_title",
            "videountertitel" => "video_untertitel",
            "videoid" => "video_id",
            "videodatum" => "teilgenommen_datum",
            "teilgenommen" => "teilgenommen",
            "strasse" => "strasse",
            "plz" => "plz",
            "ort" => "ort",
        );

        $required_export_fields = array('vorname', 'name', 'email');

        $export = array();
        foreach ($export_fields as $field => $descr)
        {
            // wenn Pflichtfeld oder Exporthaken gesetzt ist
            if (in_array($field, $required_export_fields) || $export_config[api_table_export_column_name($field)] == 1)
            {
                // übernehmen bedeutet im array zu setzen: mailer_feldname => wert_von(imparare_feldname)
                $export[$export_config[api_table_mailer_column_name($field)]] = $teilnehmer->nice_field($imparare_field_names[$field]);
            }
        }


        /*$data = array(
            "email" => $teilnehmer->field('email'),
            "merge_fields" => array(
                "NAME"=>$teilnehmer->field('name'),
                "VORNAME"=>$teilnehmer->field('vorname'),
                "TELEFON"=>$teilnehmer->field('telefon'),
                "ANREDE"=>$teilnehmer->field('anrede') == 1 ? 'Herr' : 'Frau',
                "ANSPRACHE"=>$teilnehmer->field('ansprache'),
                "IP"=>$teilnehmer->field('teilgenommen_ip'),
                "SESSION"=>$teilnehmer->field('teilgenommen_session'),
                "ZUORDNUNG"=>strtoupper($teilnehmer->field('zuordnung')),
                "FIRMA"=>$teilnehmer->field('firma'),
                "VONSEITE"=>$teilnehmer->field('video_id'),
                //"SMS"=>' ',
            ),
        );*/
        $data = array(
            "email" => $teilnehmer->field('email'),
            "global_attributes" => $export,
        );

        echo "export data:";
        echo "<pre>";
        print_r($data);
        echo "</pre>";

        try
        {
            require_once ('clever_reach_rest_client.php');
            $rest = new CR\tools\rest("https://rest.cleverreach.com/v2");
            $token = $rest->post('/login',
                array(
                    "client_id"=>$this->api_id,
                    "login"=>$this->api_username,
                    "password"=>$this->api_password
                )
            );
            $rest->setAuthMode("bearer", $token);
            $res_dec = $rest->post("/groups/{$this->list_id}/receivers", $data);

            //echo "<pre>";
            //print_r($res_dec);
            //echo "</pre>";
        }
        catch (Exception $e)
        {
            echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
        }

        return true;
    }

    function post_receiver_to_list($receiver_mail, $list_id)
    {

        $data = array(
            "email" => $receiver_mail,
        );

        try
        {
            require_once ('clever_reach_rest_client.php');
            $rest = new CR\tools\rest("https://rest.cleverreach.com/v2");
            $token = $rest->post('/login',
                array(
                    "client_id"=>$this->api_id,
                    "login"=>$this->api_username,
                    "password"=>$this->api_password
                )
            );
            $rest->setAuthMode("bearer", $token);
            $res_dec = $rest->post("/groups/{$list_id}/receivers", $data);

            //echo "<pre>";
            //print_r($res_dec);
            //echo "</pre>";
        }
        catch (Exception $e)
        {
            /*echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";*/
        }
    }

    function get_first_list_id()
    {
        /*$ch = curl_init('https://us16.api.mailchimp.com/3.0/lists');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->mc_api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);
        return $res_dec->lists[0]->id;*/
    }

    function get_lists()
    {
        try
        {
            require_once ('clever_reach_rest_client.php');
            $rest = new CR\tools\rest("https://rest.cleverreach.com/v2");
            $token = $rest->post('/login',
                array(
                    "client_id"=>$this->api_id,
                    "login"=>$this->api_username,
                    "password"=>$this->api_password
                )
            );
            $rest->setAuthMode("bearer", $token);
            $res_dec = $rest->get("/groups");

            //echo "GRUPPEN<br/>";
            //echo "<pre>";
            //print_r($res_dec);
            //echo "</pre>";

            return $res_dec;
        }
        catch (Exception $e)
        {
            echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
        }
    }

    function get_fields($group_id)
    {
        try
        {
            require_once ('clever_reach_rest_client.php');
            $rest = new CR\tools\rest("https://rest.cleverreach.com/v2");
            $token = $rest->post('/login',
                array(
                    "client_id"=>$this->api_id,
                    "login"=>$this->api_username,
                    "password"=>$this->api_password
                )
            );
            $rest->setAuthMode("bearer", $token);

            $ret = array();

            $res_dec = $rest->get("/attributes");
            if (is_array($res_dec))
            {
                foreach ($res_dec as $item)
                {
                    $entry = array();
                    $entry['name'] = $item->name;
                    $entry['description'] = $item->description;
                    array_push($ret, $entry);
                }
            }

            $res_dec = $rest->get("/groups/$group_id/attributes");
            if (is_array($res_dec))
            {
                foreach ($res_dec as $item)
                {
                    $entry = array();
                    $entry['name'] = $item->name;
                    $entry['description'] = $item->description;
                    array_push($ret, $entry);
                }
            }

            return $ret;
        }
        catch (Exception $e)
        {
            echo "ERROR";
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
        }
    }
}


function api_table_export_column_name($fieldname)
{
    return "p_".$fieldname."_export";
}
function api_table_imparare_column_name($fieldname)
{
    return "p_".$fieldname."_imparare";
}
function api_table_mailer_column_name($fieldname)
{
    return "p_".$fieldname."_mailer";
}

