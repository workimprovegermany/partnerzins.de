<?php


class Entity_load_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.load');
    }

    public function execute()
    {
        /* zwei Möglichkeiten für Entity Load
         *
         * 1. id wird übermittelt
         * 2. Filter werden übermittelt
         *
         * außerdem können mit entity_fields die Felder eingeschränkt werden, die übermittelt werden.
         */

        $event = new Event();

        //echo "hello from entity load action <br/>\n";
        //echo "data:\n";
        //print_r($this->data);
        //echo "<br/>\n";

        $app = App::get_instance();

        $app_config = $app->config();
        $entity_config = $app_config->entity_config_for_name($this->data['entity_name']);
        $em = new Entity_mapper($app->db(), $entity_config);
        $ec = new Entity_config($entity_config);

        $entities = array();
        if (isset($this->data['entity_id']))
        {
            array_push($entities, $em->find_by_id($this->data['entity_id']));
        }
        else
        {
            $entities = $em->find_by_fields($this->data['entity_filter']);
        }

        $filter_fields = isset($this->data['entity_fields']) ? $this->data['entity_fields'] : $ec->get_fields();

        $return_entities = array();
        if ($entities !== null)
        {
            foreach($entities as $entity)
            {
                $e = array();
                foreach ($filter_fields as $name)
                {
                    $e[$name] = $entity->field($name);
                }

                $e['id'] = $entity->id();
                array_push($return_entities, $e);
            }
        };


        $return_data = '';
        if (isset($this->data['entity_header']) && $this->data['entity_header'] = 'true')
        {
            $return_header = array();
            foreach ($filter_fields as $name)
            {
                $return_header[$name] = $ec->field_description($name)['type'];
            }
            $return_data = ['header' => $return_header, 'data' => $return_entities];
        }
        else
        {
            $return_data = ['data' => $return_entities];
        }

        header('Content-type: application/json');
        echo json_encode($return_data);
    }
}