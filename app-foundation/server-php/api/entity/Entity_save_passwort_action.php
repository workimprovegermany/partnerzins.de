<?php


class Entity_save_passwort_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.save_password');
    }

    public function execute()
    {
        $event = new Event();

        //echo "hello from entity update action <br/>\n";
        //echo "data:\n";
        //print_r($this->data);
        //echo "<br/>\n";
        function randomCode($length = 5) {
            $str = "";
            $char = array_merge(range('a','z'),range('A','Z'), range('0','9'));
            $max = count($char) - 1;
            for ($i = 0; $i < $length; $i++) {
                $rand = mt_rand(0, $max);
                $str .= $char[$rand];
            }
            return $str;
        }

        $app = App::get_instance();

        // 1. config_path für entity_name besorgen (steht in app_config)
        $app_config = $app->config();
        $entity_config = $app_config->entity_config_for_name($this->data['entity_name']);

        // 2. Entity_mapper initialisieren
        $em = new Entity_mapper($app->db(), $entity_config);

        // 3. Entity mit der übermittelten id (this data id) aus db laden ($em->find_by_fields)
        $fieldSess = array(
            'session'=>$this->data['entity_id'],
        );
        $entity = $em->find_by_fields($fieldSess);

        if($entity[0]->field('aktiv') == 1) {
            // 4. foreach über die übermittelten fields (this data fields) und entity aus 3. updaten
            foreach ($this->data['entity_fields'] as $name=>$value)
            {
                if($name == 'upass') {
                    $value = password_hash($value,PASSWORD_DEFAULT);
                }
                $entity[0]->set_field($name, $value);
            }
            $entity[0]->set_field('aktiv',0);
            // 5. Entity über den mapper ($em) wieder speichern
            $em->save($entity[0]);
            //echo $entity[0]->id();

            do {
                 $uid = randomCode();
                 //  Entity_mapper initialisieren
                 $ecode = new Entity_mapper($app->db(), 'user/1.0/config.xml');

                 // 3. Entity mit der übermittelten id (this data id) aus db laden ($em->find_by_fields)
                 $fieldCode = array(
                     'short_id'=>$uid,
                 );
                 $entityCode = $ecode->find_by_fields($fieldCode);
            }
            while($entityCode == 1);

            // 6. Save
            $euu = new Entity_mapper($this->app->db(),'user/1.0/config.xml');
            $eu = Entity_config::create('user/1.0/config.xml');
            $eu->set_field('anrede', $entity[0]->field('anrede'));
            $eu->set_field('nachname', $entity[0]->field('nachname'));
            $eu->set_field('affiliate_short_id', $entity[0]->field('affiliate_short_id'));
            $eu->set_field('affiliate_uid', $entity[0]->field('affiliate_uid'));
            $eu->set_field('short_id', $uid);
            //echo $entityCode."/".$uid;
            $euu->insert($eu);
            $fieldShort = array(
                'short_id'=>$uid,
                //'short_id'=>'zeHV2',
            );

            $entity_id = $euu->find_by_fields($fieldShort); // (B)
            //echo 'test - '.$entity_id[0]->id();

            $eul = new Entity_mapper($this->app->db(),'user_login/1.0/config.xml');
            //$test = $eul->find_by_fields('uid',1,'','uid',DESC);
            $e = Entity_config::create('user_login/1.0/config.xml');
            $e->set_field('uname', $entity[0]->field('uname'));
            $e->set_field('upass', $entity[0]->field('upass'));
            $e->set_field('uid', $entity_id[0]->id()); // (B)
            $eul->insert($e);


            $epr = new Entity_mapper($this->app->db(),'user_provision_struktur/1.0/config.xml');
            $ep = Entity_config::create('user_provision_struktur/1.0/config.xml');
            $ep->set_field('affiliate_uid',$entity_id[0]->id());
            $ep->set_field('u_provision','27.0000');
            $ep->set_field('u_provision_id','1');
            $ep->set_field('uid',$entity_id[0]->id());
            $epr->insert($ep);

            $ep->set_field('affiliate_uid',$entity[0]->field('affiliate_uid'));
            $ep->set_field('u_provision','8.0000');
            $ep->set_field('u_provision_id','2');
            $ep->set_field('uid',$entity_id[0]->id());
            $epr->insert($ep);

            $event->set_name('entity.saved')->set_data($this->data['entity_name']);
        }
        else{
            $event->set_name('entity.error')->set_data('inaktiv');
        }

        header('Content-type: application/json');
        echo $event->to_json();
    }
}