<?php


class Entity_save_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.save');
    }

    public function execute()
    {
        $event = new Event();

        //echo "hello from entity update action <br/>\n";
        //echo "data:\n";
        //print_r($this->data);
        //echo "<br/>\n";

        $app = App::get_instance();

        $app_config = $app->config();
        $entity_config_path = $app_config->entity_config_for_name($this->data['entity_name']);

        $e = Entity_config::create($entity_config_path);
        foreach ($this->data['entity_fields'] as $name => $value)
        {
            $e->set_field($name, $value);
        }

        $em = new Entity_mapper($app->db(), $entity_config_path);
        $em->insert($e);

        $event = new Event();
        $event->set_name('entity.saved')->set_data($this->data['entity_name']);

        header('Content-type: application/json');
        echo $event->to_json();
    }
}