<?php


class Entity_update_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.update');
    }

    public function execute()
    {
        $event = new Event();

        //echo "hello from entity update action <br/>\n";
        //echo "data:\n";
        //print_r($this->data);
        //echo "<br/>\n";

        $app = App::get_instance();

        // 1. config_path für entity_name besorgen (steht in app_config)
        $app_config = $app->config();
        $entity_config = $app_config->entity_config_for_name($this->data['entity_name']);

        // 2. Entity_mapper initialisieren
        $em = new Entity_mapper($app->db(), $entity_config);

        // 3. Entity mit der übermittelten id (this data id) aus db laden ($em->find_by_fields)
        $entity = $em->find_by_id($this->data['entity_id']);

        // 4. foreach über die übermittelten fields (this data fields) und entity aus 3. updaten
        foreach ($this->data['entity_fields'] as $name=>$value)
        {
            $entity->set_field($name, $value);
        }

        // 5. Entity über den mapper ($em) wieder speichern
        $em->save($entity);
        $event->set_name('entity.saved')->set_data($this->data['entity_name']);

        header('Content-type: application/json');
        echo $event->to_json();
    }
}