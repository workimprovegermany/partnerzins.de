<?php


class Entity_delete_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.delete');
    }

    public function execute()
    {
        /*
         * gelöscht wird vom Modell entity_name die ID entity_id
         */

        $event = new Event();

        //echo "hello from entity load action <br/>\n";
        //echo "data:\n";
        //print_r($this->data);
        //echo "<br/>\n";

        $app = App::get_instance();

        $app_config = $app->config();
        $entity_config = $app_config->entity_config_for_name($this->data['entity_name']);
        $em = new Entity_mapper($app->db(), $entity_config);
        $em->delete($this->data['entity_id']);

        $event->set_name('entity.deleted')->set_data($this->data['entity_name']);
        header('Content-type: application/json');
        echo $event->to_json();
    }
}