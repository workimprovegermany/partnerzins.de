<?php

/*
 * Mail config is located in app_config.xml, mail template or mail.send event.
 * Mail config fields in app_config.xml are overwritten by mail template and mail.send event.
 */
class Mail_send_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('mail.send');
    }

    public function execute()
    {
        // fill $mail_settings with config and overwrite with template and event

        $mail_settings = array();

        if (isset($this->data['config']))
        {
            $mail_settings = $this->app->config()->mail_settings($this->data['config']);
        }

        $view = View::from_JSON(json_encode($this->data['template']));
        $template_data = $this->data['template'][1];
        foreach ($template_data as $entity_name => $entity_id)
        {
            $entity_config_path = $this->app->config()->entity_config_for_name($entity_name);

            $em = new Entity_mapper($this->app->db(), $entity_config_path);
            $ret = $em->find_by_fields(array('id' => $entity_id));

            if ($ret !== null)
            {
                $entity = $ret[0];
                $view->assign($entity_name, $entity);
                //print_r($entity);
                //print_r($view);
            }
        }
        $mail_body = $view->render_body();
        $mail_txt = $view->get_mail_text();

        $template_settings = $view->get_mail_settings();
        foreach ($template_settings as $k=>$v)
        {
            $mail_settings[$k] = $v;
        }


        foreach ($this->data as $k=>$v)
        {
            $mail_settings[$k] = $v;
        }

        $mail = new Mail();
        $mail->set_host($mail_settings['host']);
        $mail->set_auth($mail_settings['user'], $mail_settings['pass']);
        $mail->set_from($mail_settings['from']);
        $mail->set_from_name($mail_settings['from_name']);
        //TODO: adress can be array
        $mail->add_address($mail_settings['address']);
        //TODO: add bcc like adress

        $mail->set_subject(utf8_decode($mail_settings['subject']));
        $mail->set_body(utf8_decode($mail_body));
        $mail->set_alt_body($mail_txt);

        if (!$mail->send()) {
            echo "<br /><p class=\"error\">Message was not sent ! <br /> Mailer Error: " . $mail->ErrorInfo . "</p>";
        }


    }

}