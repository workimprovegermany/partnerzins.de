<?php

namespace AF;

abstract class Action
{
    /**
     * @var \App
     */
    protected $app = null;

    protected $data = array();
    protected $user_name = null;
    protected $user_id = null;
    protected $ip = '';
    protected $session_id = '';


    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * @param null $app
     */
    public function set_app($app)
    {
        $this->app = $app;
    }

    /**
     * @param null $user_id
     */
    public function set_user($user_name, $user_id)
    {
        $this->user_name = $user_name;
        $this->user_id = $user_id;
    }

    /**
     * @param string $session_id
     * @param string $ip
     */
    public function set_session($session_id, $ip)
    {
        $this->session_id = $session_id;
        $this->ip = $ip;
    }



    abstract public function execute();
}