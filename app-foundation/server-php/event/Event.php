<?php

class Event
{
    /*
     * event name is a unique name.
     * it consists of lowercase letters, numbers, underscores and dots.
     *
     * dots are used to build namespaces for events.
     */
    private $name = 'system.dummy_event';

    /*
     * event data
     */
    private $data = null;

    /**
     * @return string|null
     */
    public function get_name()
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Event
     */
    public function set_name($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null
     */
    public function get_data()
    {
        return $this->data;
    }

    /**
     * @return null
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * @param null $data
     * @return Event
     */
    public function set_data($data)
    {
        $this->data = $data;
        return $this;
    }




    public function __construct($name = null, $data = null)
    {
        $this->name = $name;
        $this->data = $data;
    }

    public function to_json()
    {
        $array = ['name' => $this->name, 'data' => $this->data];
        return json_encode($array);
    }

    public static function from_json($event_json)
    {
        $event_array = json_decode($event_json, true);

        $e = new Event($event_array['name'], $event_array['data']);
        return $e;
    }

}