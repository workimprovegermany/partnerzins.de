<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

// TODO move imparare code
//session_name("WEBWEBINAR");
//session_start();
//$session_id = session_id($_COOKIE["WEBWEBINAR"]);
//$ip = $_SERVER['REMOTE_ADDR'];

require_once('foundation.php');

$app_config = new Config(APP_CONFIG);
$app_name = $app_config->entry('app_name').'App';
require_once(APP_ROOT_PATH.'/'.$app_name.'.php');
$app = $app_name::get_instance();

// TODO move imparare code
// TODO error handling
/*$st = $app->db()->query("SELECT `uname`,`lang` FROM `afh_online` WHERE `session_id`='$session_id' AND `loggedin`='1' LIMIT 1");
$res = $app->db()->fetch_array($st);
$user_name = $res['uname'];

$st = $app->db()->query("SELECT * FROM `afh_user_login` WHERE `uname`='$user_name' LIMIT 1");
$res = $app->db()->fetch_array($st);
$user_id = intval($res['uid']);*/

$event = null;

if (isset($_POST['event']))
{
    $event_json = is_array($_POST['event']) ? json_encode($_POST['event']) : $_POST['event'];
    $event = Event::from_json($event_json);
}
else
{
    //legacy!!!

    // the cmd is in one of the following locations: POST, GET, URL

    $cmd = isset($_POST['af_cmd']) ? $_POST['af_cmd'] : null;
    $data = isset($_POST['af_data']) ? json_decode($_POST['af_data'],true) : null;

    //file_put_contents('api.log',"postvar: ".print_r($_POST,true),FILE_APPEND);

    if (empty($cmd))
    {
        $cmd = isset($_GET['af_cmd']) ? $_GET['af_cmd'] : null;
        $data = isset($_GET['af_data']) ? json_decode($_GET['af_data'],true) : null;
    }

    if (empty($cmd))
    {
        $url_cmd = isset($_GET['af_cmdu']) ? $_GET['af_cmdu'] : null;
        if (!empty($url_cmd))
        {
            $parts = explode('/',$url_cmd,2);
            $cmd = $parts[0];
            $data = $parts[1];
        }
    }

    $event = new Event($cmd, $data);
}

$app->dispatch_event($event);