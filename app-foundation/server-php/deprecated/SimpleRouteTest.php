<?php

//Routingtest

include('php/SimpleRoute.php');
SimpleRoute::init();
//base route
SimpleRoute::add_route('',function(){
    //Do something
    echo 'Welcome :-)';
});

//simple route
SimpleRoute::add_route('test.html',function(){
    //Do something
    echo 'Hello from test.html';
});

//complex route with parameter
SimpleRoute::add_route('user/(.*)/edit',function($id){
    //Do something
    echo 'Edit user with id '.$id;
});

SimpleRoute::add_route_err(function($url){
    //Send 404 Header
    header("HTTP/1.0 404 Not Found");
    echo '404 :-(';
});

SimpleRoute::run();
