<?php


class Query
{
    public static function build_query_from_filters($table, $filter_array, $offset, $limit)
    {
        $query = "SELECT id FROM $table";
        $query .= self::build_query_where_clause_from_filters($filter_array);
        $query .= " ORDER BY last_mod DESC LIMIT $offset,$limit";
        return $query;
    }

    public static function build_query_from_filters_temp($table, $filter_string, $offset, $limit)
    {
        $query = "SELECT id FROM $table";

        if (!empty($filter_string))
        {
            $parts = explode(':', $filter_string);
            $where = " WHERE ";
            for ($p = 0; $p < count($parts); $p+=2)
            {
                $where .= $parts[$p].'='."'".$parts[$p+1]."'";
            }
        }

        $query .= $where." ORDER BY last_mod DESC LIMIT $offset,$limit";

        return $query;
    }

    public static function build_count_query_from_filters($table, $filter_array)
    {
        $query = "SELECT COUNT(*) FROM $table";
        $query .= self::build_query_where_clause_from_filters($filter_array);
        return $query;
    }

    public static function build_query_where_clause_from_filters($filter_array)
    {
        $firstfilter=true;
        $where = '';

        foreach ($filter_array as $filter)
        {
            if ($filter->has_value)
            {
                if ($firstfilter)
                {
                    $where .= " WHERE ";
                    $firstfilter = false;
                }
                else
                {
                    $where .= " AND ";
                }

                if ($filter->value_type  == 'range')
                {
                    $where .= $filter->name . " > " . $filter->value[0] * 100;
                    $where .= " AND ";
                    $where .= $filter->name ." < " . $filter->value[1] * 100;
                }
                elseif ($filter->value_type  == 'absolute')
                {
                    $where .= $filter->name . " = " . "'" . $filter->value . "'";
                }
                elseif ($filter->value_type  == 'bool')
                {
                    $where .= $filter->name . " = " . $filter->value;
                }
            }
        }
        return $where;
    }

}
