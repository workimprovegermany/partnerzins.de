<?php

class View
{
    private $template_file;
    private $template_file_txt;
    private $data;
    private $template;
    private $template_txt;
    private $left_delimiter  = '{{';
    private $right_delimiter = '}}';

    public function __construct($file = null)
    {
        $this->template_file = $file;

        $txt_file = pathinfo($file, PATHINFO_DIRNAME).'/'.pathinfo($file, PATHINFO_FILENAME).'.txt';
        $full_txt_path = APP_ROOT_PATH.'/views/'.$txt_file;
        $this->template_file_txt = file_exists($full_txt_path) ? $txt_file : null;

        $this->load_template_file();
    }

    private function load_template_file()
    {
        //TODO: View provider, der templates aus dem richtigen Verzeichnis laden kann. private $view_provider;
        $full_path = APP_ROOT_PATH.'/views/'.$this->template_file;
        if (!empty($this->template_file) && file_exists($full_path))
        {
            $this->template = file_get_contents($full_path);
        }

        if ($this->template_file_txt !== null)
        {
            $full_txt_path = APP_ROOT_PATH.'/views/'.$this->template_file_txt;
            $this->template_txt = file_get_contents($full_txt_path);
        }
    }

    public function set_template($template)
    {
        $this->template_file = null;
        $this->template = $template;
    }

    public function replace($token, $replacement)
    {
        $search = $this->left_delimiter . $token . $this->right_delimiter;
        if (strpos($this->template, $search) !== false)
        {
            $this->template = str_replace($search, $replacement, $this->template);
        };
        return $this->template;
    }

    public function get_text()
    {
        return $this->template;
    }

    public function assign($name, $data)
    {
        $this->data[$name] = $data;
    }

    public function render()
    {
        $this->load_template_file();

        echo "<br/><br/><br/>";
        //echo $this->template;

        $matches = array();
        while (preg_match('${{(?<entity>.*)\.(?<attribute>.*)}}$', $this->template,$matches, PREG_OFFSET_CAPTURE) === 1)
        {
            $new_template = substr($this->template, 0, $matches[0][1]);
            $new_template .= $this->data[$matches[1][0]]->field($matches[2][0]);
            $new_template .= substr($this->template, $matches[0][1] + strlen($matches[1][0]) + strlen($matches[2][0]) + 5);

            $this->template = $new_template;

            echo "<br/><br/><br/>";
            //print_r($matches);
        }

        echo "<br/><br/><br/>";
        //echo $this->template;
    }

    public function render_body()
    {
        $this->load_template_file();

        $matches = array();
        while (preg_match('${{(?<entity>[a-z_]*)\.(?<attribute>[a-z_]*)}}$', $this->template,$matches, PREG_OFFSET_CAPTURE) === 1)
        {
            $new_template = substr($this->template, 0, $matches[0][1]);
            if ($matches[2][0] == 'id')
            {
                $new_template .= $this->data[$matches[1][0]]->id();
            }
            else
            {
                if ($matches[1][0] == '_af')
                {
                    switch ($matches[2][0])
                    {
                        case 'datum':
                            $new_template .= date('d.m.Y');
                            break;
                    }
                }
                elseif (isset($this->data[$matches[1][0]]))
                {
                    $new_template .= $this->data[$matches[1][0]]->field($matches[2][0]);
                }
                else
                {
                    $m = $matches[1][0];
                    echo "$m not assigned to View <br/>\n";
                    print_r($matches);
                    echo "<br/>\n";
                }

            }
            $new_template .= substr($this->template, $matches[0][1] + strlen($matches[1][0]) + strlen($matches[2][0]) + 5);

            $this->template = $new_template;
        }

        $dd = new DOMDocument();
        $dd->loadHTML($this->template);
        $body = $dd->getElementsByTagName('body')[0];
        return $dd->saveHTML($body);
    }

    public function get_mail_settings()
    {
        $dd = new DOMDocument();
        $dd->loadHTML($this->template);
        $meta = $dd->getElementById('mailsettings');

        $mail_settings = array();

        if ($meta->hasAttribute('data-address')) $mail_settings['address'] = $meta->getAttribute('data-address');
        if ($meta->hasAttribute('data-subject')) $mail_settings['subject'] = $meta->getAttribute('data-subject');

        return $mail_settings;
    }

    public function get_mail_text()
    {
        if ($this->template_file_txt == null) return '';

        $matches = array();
        while (preg_match('${{(?<entity>[a-z_]*)\.(?<attribute>[a-z_]*)}}$', $this->template_txt,$matches, PREG_OFFSET_CAPTURE) === 1)
        {
            $new_template = substr($this->template_txt, 0, $matches[0][1]);

            if ($matches[1][0] == '_af')
            {
                switch ($matches[2][0])
                {
                    case 'datum':
                        $new_template .= date('d.m.Y');
                        break;
                }
            }
            elseif (isset($this->data[$matches[1][0]]))
            {
                $new_template .= $this->data[$matches[1][0]]->field($matches[2][0]);
            }
            else
            {
                $m = $matches[1][0];
                echo "$m not assigned to View <br/>\n";
                print_r($matches);
                echo "<br/>\n";
            }



            $new_template .= substr($this->template_txt, $matches[0][1] + strlen($matches[1][0]) + strlen($matches[2][0]) + 5);

            $this->template_txt = $new_template;
        }

        return $this->template_txt;
    }

    public static function from_JSON($json)
    {
        $tpl = json_decode($json,true);
        $tname = $tpl[0];
        $tdata = $tpl[1];

        $tpl = new View($tname);

        $app = App::get_instance();

        foreach ($tdata as $entity_name=>$entity_id)
        {
            $em = new Entity_mapper($app->db(),$entity_name.'/1.0/config.xml');
            $fields = array(
                'id' => $entity_id,
            );
            $user_login_array = $em->find_by_fields($fields, 1);
        }
        return $tpl;
    }
}