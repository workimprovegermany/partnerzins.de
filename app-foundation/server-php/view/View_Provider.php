<?php

class View_Provider
{
    private $path;

    public function __construct($path){
        $this->path = $path;
    }

    public function load($view_name){
        if( file_exists($this->path.$view_name) ){
            return file_get_contents($this->path.$view_name);
        }
        throw new Exception("View does not exist: ".$this->path.$view_name);
    }
}