<?php
define('BASEPATH', __DIR__);
require('php-foundation/autoload/Autoload.php');

// autoloaded
//require('php-foundation/view/View_Provider.php');
//require('php-foundation/view/View.php');

$autoloader = new Autoload();

spl_autoload_register([$autoloader, 'load']);

$autoloader->register('viewprovider', function(){
    return require(BASEPATH.'/core/view/View_Provider.php');
});

$view = new View( new View_Provider(BASEPATH.'/views/') );

/*$view_path = __DIR__.'/views/';

$view_provider = new View_Provider($view_path);
$view = new View($view_provider);*/

$view->show('hello.php');





