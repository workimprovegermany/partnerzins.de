<?php

class Amazon_PA
{
    private $config_file;

    private $aws_associate_tag = '';
    private $aws_access_key_id = '';
    private $aws_secret_key = '';
    private $endpoint = '';

    public function __construct($config_file)
    {
        $this->config_file = $config_file;
        $this->load_config_data();
    }

    private function load_config_data()
    {
        $config = new Xml_document($this->config_file);

        $this->aws_associate_tag = $config->query_string("/config/amazon/aws_associate_tag");
        $this->aws_access_key_id = $config->query_string("/config/amazon/aws_access_key_id");
        $this->aws_secret_key = $config->query_string("/config/amazon/aws_secret_key");
        $this->endpoint = $config->query_string("/config/amazon/endpoint");
    }

    public function item_lookup($asins)
    {
        //TODO: max 10 asins in one request
        $uri = "/onca/xml";

        if (is_array($asins))
        {
            $asin = implode(",", $asins);
        }
        else
        {
            $asin = $asins;
        }

        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemLookup",
            "AWSAccessKeyId" => $this->aws_access_key_id,
            "AssociateTag" => $this->aws_associate_tag,
            "ItemId" => $asin,
            "IdType" => "ASIN",
            "ResponseGroup" => "Images,ItemAttributes,Offers,EditorialReview,SalesRank"
        );

        // Set current timestamp if not set
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }

        // Sort the parameters by key
        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
        }

        // Generate the canonical query
        $canonical_query_string = join("&", $pairs);

        // Generate the string to be signed
        $string_to_sign = "GET\n".$this->endpoint."\n".$uri."\n".$canonical_query_string;

        // Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->aws_secret_key, true));

        // Generate the signed URL
        $request_url = 'http://'.$this->endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $xml_response = curl_exec($ch);

        return $xml_response;
    }

    public function item_search($search_str)
    {
        $uri = "/onca/xml";

        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemSearch",
            "AWSAccessKeyId" => $this->aws_access_key_id,
            "AssociateTag" => $this->aws_associate_tag,
            "SearchIndex" => "All",
            "Keywords" => $search_str,
            "ResponseGroup" => "Images,ItemAttributes,Offers"
        );

        // Set current timestamp if not set
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }

        // Sort the parameters by key
        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
        }

        // Generate the canonical query
        $canonical_query_string = join("&", $pairs);

        // Generate the string to be signed
        $string_to_sign = "GET\n" . $this->endpoint . "\n" . $uri . "\n" . $canonical_query_string;

        // Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->aws_secret_key, true));

        // Generate the signed URL
        $request_url = 'http://' . $this->endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $xml_response = curl_exec($ch);

        return $xml_response;
    }

    public static function make_link($str)
    {
        return "<a href=\"$str\" class=\"btn btn-default\" role=\"button\" rel=\"nofollow\">bei amazon kaufen</a>";
    }
}