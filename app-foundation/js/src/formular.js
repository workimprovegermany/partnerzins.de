$(function() {
    $('.js-show-password').on('click', function () {
        const $element = $(this).parent().find('input'),
            type = $element.attr('type');

        if("password" === type){
            $element.attr('type', 'text');
        } else {
            $element.attr('type', 'password');
        }
    });

    $('.js-check-password-repeat').on('change', function () {
        if($(this).val() !== $('.js-check-password').val()){
            $(this).next().show();
        } else {
            $(this).next().hide();
        }
    });

    $('.js-check-password').on('change', function () {
        const valuePass = $(this).val();
        if('' !== valuePass){
            $('.js-check-password-repeat').attr('pattern', valuePass);
        } else {
            $('.js-check-password-repeat'). removeAttr("pattern");
        }

    });


    $('.js-save-passwort').on('click', function(ev){
        var valid = this.form.checkValidity();
        if(valid) {
            ev.preventDefault();

            var anrede = $('#anrede').val(),
                nachname = $('#nachname').val(),
                newPassword = $('#newPassword').val(),
                id = $('#anm_id').val();

            //DATEN SENDEN
            $.ajax({
                url: 'api',
                type: 'POST',
                data: {
                    event: {
                        name: 'entity.save_password',
                        data: {
                            entity_name: 'user_preaccount',
                            entity_id: id,
                            entity_schluessel: 'email',
                            entity_fields: {
                                anrede: anrede,
                                nachname: nachname,
                                upass: newPassword,
                            }
                        }
                    },
                },
                success: function (response) {
                    console.log(response);
                    //window.location.href = 'http://192.168.240.113/partnerzins.de/password_erfolgreich.php';
                    window.location.href = 'http://partnerzins-online.de/password_erfolgreich.php';

                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
    });




});