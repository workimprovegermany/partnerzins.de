<?php
    $id = $_GET['id'];
    require_once("WincentApp.php");

    $app = WincentApp::get_instance();
    $ent = new Entity_mapper($app->db(), 'user_preaccount/1.0/config.xml');
    $fieldsSess = array(
        'session' => $id,
    );
    $data_user = $ent->find_by_fields($fieldsSess);
   // print_r($data_user);




?>

    <!DOCTYPE html>
    <html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MONEYWELL © | Online Geld verdienen</title>
        <link rel="icon" type="image/x-icon" href="img/icon.ico">

        <link rel="stylesheet" href="app-foundation/css/dist/af.css">
        <link rel="stylesheet" href="css/dist/style.css">
        <script src="vendor/dist/vendor.min.js"></script>
        <script src="app-foundation/js/dist/af.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />

        <link rel='stylesheet' id='cookie-notice-front-css'  href='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.8.1' type='text/css' media='all' />
        <script type='text/javascript'>
            /* <![CDATA[ */
            var cnArgs = {"ajaxurl":"https:\/\/www.moneywell-vertrieb.de\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"no","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":""};
            /* ]]> */
        </script>
        <script type='text/javascript' src='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.42'></script>
    </head>
    <body>
    <main>
        <section class="bg-basiccolor">
            <div class="container-fluid">
                <div class="grid_12 center pad20">
                    <h2 class="uppercase">Online Geld verdienen mit dem richtigen Partner&shy;programm</h2>
                </div>
            </div>
        </section>


        <section>
            <div class="container-fluid">
                <div class="grid_12 pad300">
                    <div class="grid_10 inline-block marg012-pro" style="width: 75%;">
                       <div class='infobox_zahlen'>
                           <div>
                               <div class='icon_pfeil'></div>
                            </div>
                       <div>
                           <p>Vielen Dank für Ihre Anmeldung! Klicken Sie auf den Button „ZUM LOGIN“ um das Partnerprogramm zu starten. Vervollständigen Sie im internen Bereich dann innerhalb von 24 Stunden Ihre Registrierung um mit dem Partnerprogramm erfolgreich durchzustarten.</p>
                       </div>
                    </div>

                    </div>
                    <div class="grid_10 inline-block marg012-pro" style="width: 75%;">
                        <div class="grid_12 marg 150">
                            <p><strong>Die Anmeldung im Partnerprogramm von Moneywell „Wincent“ ist erfolgreich abgeschlossen! </strong></p>
                        </div>
                    </div>
                    <div class="grid_10 inline-block center marg3012-pro" style="width: 75%;">
                        <!--<a href="http://192.168.240.113/wincent.de" class="btn_04">zum Login</a>-->
                        <a href="http://wincent-online.de" class="btn_04">zum Login</a>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <footer>
        <p>Moneywell<sup>©</sup>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://www.moneywell-vertrieb.de/impressum/" target="_blank">Impressum</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
                    href="https://www.moneywell-vertrieb.de/datenschutz/" target="_blank">Datenschutz</a></p>
    </footer>


    <div id="cookie-notice" role="banner" class="cn-top wp-default" style="color: #fff; background-color: #a4a4a4;">
        <div class="cookie-notice-container">
            <span id="cn-notice-text">Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren und die Zugriffe auf unsere Website zu analysieren. Außerdem geben wir Informationen zu Ihrer Nutzung unserer Website an unsere Partner für soziale Medien, Werbung, Analysen und an die Moneywell Vertriebsgesellschaft GmbH weiter.
                <a href="https://www.moneywell-vertrieb.de/datenschutz/" style="color: #ff6a00;" target="_blank">Details.</a>
            </span>
            <a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie button wp-default" style="background: #ff6a00; color: white;">OK</a>
        </div>
    </div>

    </body>
    </html>

