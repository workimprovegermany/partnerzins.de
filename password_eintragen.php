<?php
    $id = $_GET['id'];
    require_once("WincentApp.php");

    $app = WincentApp::get_instance();
    $ent = new Entity_mapper($app->db(), 'user_preaccount/1.0/config.xml');
    $fieldsSess = array(
        'session' => $id,
    );
    $data_user = $ent->find_by_fields($fieldsSess);
   // print_r($data_user);

    if($data_user[0]->field('bestaetigt') == 0) {
        $data_user[0]->set_field('bestaetigt',1);
        $ent->save($data_user[0]);
    }

  //  if($data_user[0]->field('aktiv') == 1) {
?>

    <!DOCTYPE html>
    <html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Partnerzins - Passwort</title>
        <link rel="icon" type="image/x-icon" href="img/icon.ico">

        <link rel="stylesheet" href="app-foundation/css/dist/af.css">
        <link rel="stylesheet" href="css/dist/style.css">
        <script src="vendor/dist/vendor.min.js"></script>
        <script src="app-foundation/js/dist/af.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />

        <link rel='stylesheet' id='cookie-notice-front-css'  href='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.8.1' type='text/css' media='all' />
        <script type='text/javascript'>
            /* <![CDATA[ */
            var cnArgs = {"ajaxurl":"https:\/\/www.moneywell-vertrieb.de\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"no","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":""};
            /* ]]> */
        </script>
        <script type='text/javascript' src='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.42'></script>
    </head>
    <body>
    <main>
        <section class="bg-basiccolor">
            <div class="container-fluid">
                <div class="grid_12 center pad20">
                    <h2 class="uppercase">Online Geld verdienen mit dem richtigen Partner&shy;programm</h2>
                </div>
            </div>
        </section>


        <section>
            <div class="container-fluid">
                <div class="grid_12 pad300">
                    <form>

                    <div class="grid_6 pad0-t" style="display: inline-block; margin: 0 25%; width: 50%;">
                        <div class="grid_12 marg 150">
                            <p>Mit der Anmeldung zum Moneywell-Partnerprogramm schließen Sie einen Partnervertrag mit der Moneywell Vertriebsgesellschaft mbH.</p>
                        </div>
                    </div>
                    <div class="grid_6 pad0-t" style="display: inline-block; margin: 0 25%; width: 50%;">
                        <div class="grid_12 marg 150">
                            <p style="font-size: 10px;">Hiermit willige ich ein,</p>
                        </div>
                    </div>
                    <div class="grid_6 pad0-t" style="display: inline-block; margin: 0 25%; width: 50%;">
                        <div class="grid_12 marg150 flex_center">
                            <input type="checkbox" name="check-01" id="check-01" class="input-check" required>
                            <label for="check-01" style="font-size: 10px;">Informationen über Kapitalanlagen, Projekt-Updates sowie plattformrelevante Informationen per Newsletter zu erhalten. Diese freiwillige Einwilligung kann ich jederzeit widerrufen. Zusätzlich stimme ich der <a class="underline" href="https://www.moneywell-vertrieb.de/datenschutz/" target="_blank">Datenschutzerklärung</a> der Moneywell Vertriebsgesellschaft mbH zu.
                            </label>
                        </div>
                    </div>
                    <div class="grid_6 pad0-t" style="display: inline-block; margin: 0 25%; width: 50%;">
                        <div class="grid_12 marg150 flex_center">
                            <input type="checkbox" name="check-02" id="check-02" class="input-check" required>
                            <label for="check-02" style="font-size: 10px;">Informationen über Kapitalanlagen, Projekt-Updates sowie plattformrelevante Informationen per Newsletter zu erhalten. Diese freiwillige Einwilligung kann ich jederzeit widerrufen. Zusätzlich stimme ich den <a class="underline" href="https://www.moneywell.de/rechtliches/agb" target="_blank">AGB</a> und der <a class="underline" href="https://www.moneywell.de/rechtliches/datenschutz" target="_blank">Datenschutzerklärung</a> der Moneywell GmbH zu.
                            </label>
                        </div>
                    </div>
                    <div class="grid_6 pad0-t" style="display: inline-block; margin: 0 25%; width: 50%;">
                        <div class="grid_12 marg150 flex_center">
                            <input type="checkbox" name="check-03" id="check-03" class="input-check" required>
                            <label for="check-03" style="font-size: 10px;">Sie sind bin mit der Weitergabe Ihrer personenbezogenen Daten von der Moneywell Vertriebsgesellschaft mbH an die Moneywell GmbH zum Zwecke der Verwaltung Ihrer personenbezogenen Daten und der Abrechnung Ihrer Vergütung einverstanden.
                            </label>
                        </div>
                    </div>

                    <div class="grid_6 pad0-t" style="display: inline-block; margin: 0 25%; width: 50%;">
                        <div class="grid_12 marg150 flex_center">
                            <input type="checkbox" name="check-04" id="check-04" class="input-check" required>
                            <label for="check-04" style="font-size: 10px;">Mit den Bestimmungen zu Cookies & Internet-Werbung erkläre ich mich einverstanden.</label>
                        </div>
                    </div>

                    <div class="grid_6 pad0-t" style="display: inline-block; margin: 0 25%; width: 50%;">
                        <div id="passform">

                            <div class="grid_12 marg150">
                                <select name="anrede" id="anrede">
                                    <option value="1">Herr</option>
                                    <option value="2">Frau</option>
                                </select>
                            </div>
                            <div class="grid_12 marg50">
                                <input type="text" name="nachname" id="nachname" placeholder="Nachname" required>
                            </div>
                            <div class="grid_12 marg50">
                                <input type="password" name="newPassword" id="newPassword" class="js-check-password"
                                       placeholder="Passwort eintragen" required>
                                <div class="js-show-password"></div>
                            </div>
                            <div class="grid_12 marg50">
                                <input type="password" name="newPassword2" id="newPassword2"
                                       class="js-check-password-repeat" placeholder="Passwort wiederholen" required>
                                <p class="error">Die Passwörter stimmen nicht überein!</p>
                            </div>
                            <input type="hidden" name="anm_id" id="anm_id" value="<?php echo $_GET['id']; ?>">

                            <button type="submit" class="btn_01 width100 js-save-passwort">Speichern</button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
        </section>

    </main>
    <footer>
        <p>Moneywell<sup>©</sup>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://www.moneywell-vertrieb.de/impressum/" target="_blank">Impressum</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
                    href="https://www.moneywell-vertrieb.de/datenschutz/" target="_blank">Datenschutz</a></p>
    </footer>


    <div id="cookie-notice" role="banner" class="cn-top wp-default" style="color: #fff; background-color: #a4a4a4;">
        <div class="cookie-notice-container">
            <span id="cn-notice-text">Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren und die Zugriffe auf unsere Website zu analysieren. Außerdem geben wir Informationen zu Ihrer Nutzung unserer Website an unsere Partner für soziale Medien, Werbung, Analysen und an die Moneywell Vertriebsgesellschaft GmbH weiter.
                <a href="https://www.moneywell-vertrieb.de/datenschutz/" style="color: #ff6a00;" target="_blank">Details.</a>
            </span>
            <a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie button wp-default" style="background: #ff6a00; color: white;">OK</a>
        </div>
    </div>

    </body>
    </html>
<?php
   /* }
    else {
        //header('Location: http://192.168.240.113/wincent.de');
        header('Location: http://wincent-online.de');
    }*/
?>

