Anmeldelink läuft ab - bitte handeln

Hallo,

vielen Dank für Ihre Anmeldung bei Moneywell, der Crowdinvesting-Plattform für Investments in Sachwerte. Wir benötigen bitte noch Ihre Anmeldebestätigung  - der Link läuft heute ab.
Uns ist Datenschutz ebenso wichtig wie Ihnen. Deshalb möchten wir Sie bitten, uns Ihre Anmeldung zu bestätigen. Sollten Sie sich nicht angemeldet haben, löschen Sie diese E-Mail einfach.

Klicken Sie auf den Link, um Ihre E-Mail Adresse zu bestätigen!
http://partnerzins-online.de/password_eintragen.php?id={{user_preaccount.session}}
Sollte der obenstehende Link in Ihrem E-Mail Postfach nicht funktionieren, dann kopieren Sie bitte diesen Link und fügen Sie ihn in Ihrem Web-Browser ein.

Mit besten Grüßen aus Hamburg
Ihr Moneywell-Team


Moneywell Vertriebsgesellschaft mbH
Englische Planke 2 | 20459 Hamburg | Tel 0911 / 323 919 – 65 | 0911 / 323 919 – 67
Geschäftsführer: Marc Schumann, André Wreth | Rechtsform: Gesellschaft mit beschränkter Haftung
Sitz: Hamburg – Amtsgericht Hamburg – HRB 148551

Datenschutzhinweise zur E-Mail-Kommunikation sind abrufbar unter www.moneywell-vertrieb.de/email-hinweis
Diese Nachricht ist vertraulich. Sollten Sie nicht der vorgesehene Empfänger sein, so bitten wir höflich um eine Mitteilung. Jede unbefugte Weiterleitung oder Fertigung einer Kopie ist unzulässig. Diese Nachricht dient lediglich dem Austausch von Informationen und entfaltet keine rechtliche Bindungswirkung. Aufgrund der leichten Manipulierbarkeit von E-Mails können wir keine Haftung für den Inhalt übernehmen.
This message is confidential and may be privileged. If you are not the intended recipient, we kindly ask you to inform the sender. Any unauthorised dissemination or copying hereof is prohibited. This message serves for information purposes only and shall not have any legally binding effect. Given that e-mails can easily be subject to manipulation, we cannot accept any liability for the content provided.
