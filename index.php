<!DOCTYPE html>
<html lang="de"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MONNEYWELL © | Partnerzins - Programm </title>
    <link rel="icon" type="image/x-icon" href="img/icon.ico">

    <link rel="stylesheet" href="app-foundation/css/dist/af.css">
    <link rel="stylesheet" href="css/dist/style.css">

    <script src="vendor/dist/vendor.min.js"></script>
    <script src="app-foundation/js/dist/af.min.js"></script>

    <link rel='stylesheet' id='cookie-notice-front-css'  href='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.8.1' type='text/css' media='all' />
    <script type='text/javascript'>
        /* <![CDATA[ */
        var cnArgs = {"ajaxurl":"https:\/\/www.moneywell-vertrieb.de\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"no","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":""};
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.42'></script>


<?php
    $code = $_GET['code'];
    // User tracking
    require_once("WincentApp.php");
    $app = WincentApp::get_instance();
    $short_id = empty($code) ? '' : $code;
    $tracking_zins = new Entity('user_tracking_zins/1.0/config.xml');
    $tracking_zins->set_field('session', session_id());
    $tracking_zins->set_field('short_id', $short_id);
    $tracking_zins->set_field('site', $_SERVER['REQUEST_URI']);
    Entity_mapper::insert_entity_to_storage($tracking_zins, $app->db());
?>



</head>
<body>
<?php
echo "<input type='hidden' name='short_id' id='short_id' value='$code' >";
?>
<main>
    <section class="bg-basiccolor margNeg">
        <div class="container-fluid">
            <div class="grid_12 center pad20">
                <h1 class="uppercase">Online Geld verdienen mit dem richtigen Partner&shy;programm</h1>
                <p class="big140 weight100">Wie Sie mit MONEYWELL solide Umsätze als Tippgeber schreiben, die wirklich für Sie automstisiert Verkaufen - versprochen!</p>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid">
            <div class="grid_12 pad150">
                <div class="grid_7">
                    <!--<iframe width="100%" src="https://www.youtube.com/embed/6VXN4vHiRKo?rel=0&amp;autoplay=1;autohide=1&amp;showinfo=0&amp;wmode=transparent" class="__web-inspector-hide-shortcut__">
                    </iframe>-->
                    <div class="mute" id="video-1">
                        <div class="js-mute">
                            <img src="img/icons/icon-ton-ffffff.svg" />
                        </div>
                        <video autobuffer autoplay muted controls>
                            <!--<source src="http://192.168.240.113/wincent.de/_media/videos/vermittler/Moneywell_Partner_werden_Intro_Akademie.MP4" type="video/mp4">-->

                            <source src="http://wincent-online.de/_media/videos/vermittler/Moneywell_Partner_werden_LiebeKollegen02_WA.MP4" type="video/mp4">
                        </video>
                    </div>
                    <div class="mute" id="video-2">
                        <div class="js-mute">
                            <img src="img/icons/icon-ton-ffffff.svg" />
                        </div>
                        <video autobuffer autoplay muted controls>
                            <!--<source src="http://192.168.240.113/wincent.de/_media/videos/vermittler/Moneywell_Partner_werden_LiebeKollegen02_WA.MP4" type="video/mp4">-->
                            <source src="http://wincent-online.de/_media/videos/vermittler/Moneywell_Partner_werden_Intro_Akademie.MP4" type="video/mp4">
                        </video>
                    </div>
                </div>
                <div class="grid_5 pad20 pad0-t">
                    <p class="big140 weiss tablet-blau">Holen Sie sich jetzt Ihren <span class="line">GRATIS</span>-Account!</p>
                    <div class="pfeile">
                        <img src="img/icons/pfeil.svg">
                        <img src="img/icons/pfeil.svg">
                    </div>
                    <form id = "anmeldeform">
                        <div class="grid_12 marg150">
                            <input type="email" name="email" id="email" placeholder="Beste geschäftliche E-Mail-Adresse" required>
                        </div>
                        <div class="grid_12">
                            <input type="checkbox" name="datenschutz" id="datenschutz" required>
                            <label for="datenschutz" class="small80">Ich habe die Datenschutzerklärung gelesen und bin mit der Verarbeitung und Nutzung meiner Daten einverstanden.</label>
                        </div>
                        <div class="grid_12 marg150">
                            <input type="checkbox" name="news" id="news" required>
                            <label for="news" class="small80">Ich willige ein, dass meine Daten für den Versand eines wiederkehrenden Newsletter verarbeitet werden dürfen.</label>
                        </div>
                        <div class="grid_12">
                            <p class="error" id="message"></p>
                        </div>
                        <button type="submit" class="btn_01 width100">Jetzt Account sichern</button>
                        <p class="small60 pad50">Voraussetzung für einen Partneraccount ist die Anmeldung zu unserem Newsletter,
                            mit dem wir Sie unmittelbar zu Ihrem Thema auf dem Laufenden halten. Die
                            Anmeldung stellt insofern das „Ticket“ zu Ihrem Partneraccount dar.</p>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-grau" data-section="1">
        <div class="container-fluid">
            <div class="grid_10 preffix_1 marg300 farbverlauf">
                <h2 class="center">Endlich: Die sichere Abkürzung um schnell und profitabel neue Kunden & Umsätze zu generieren</h2>
                <div class="pad150 marg150 grid_12">
                    <div class="round"><img src="img/icons/finger.svg" /> </div>
                    <h3 class="uppercase">Ganz einfach - wenig Aufwand</h3>
                    <p>Sie erhalten jedes Mal eine Provision, wenn der geworbene Kunde auf Moneywell investiert. Garantiert!</p>
                </div>
                <div class="pad150 grid_12">
                    <div class="round"><img src="img/icons/finger.svg" /> </div>
                    <h3 class="uppercase">Monatliche Provisionen erhalten und 5 Jahre Kundenschutz genießen</h3>
                    <p>Bestes Partner-Programm: Sämtliche Folgeinvestitionen Ihres Kunden werden Ihnen zugerechnet – und das für die nächsten 5 Jahre.</p>
                </div>
                <div class="pad150 marg150 grid_12">
                    <div class="round"><img src="img/icons/finger.svg" /> </div>
                    <h3 class="uppercase">Umsatzstarke Schaltzentrale</h3>
                    <p>Ihren vermittelten Bestand und Ihre Provisionen finden Sie stets auf Ihrem  persönlichen Dashboard. Moneywell bietet einfachste Kundenansprache durch effektive Vorlagen, die wirklich funktionieren. </p>
                </div>
                <div class="pad150 grid_12">
                    <div class="round"><img src="img/icons/finger.svg" /> </div>
                    <h3 class="uppercase">Digital, unbürokratisch, ohne Beraterhaftung</h3>
                    <p>Wir bieten eine konstante Deal-Pipeline für unsere Investoren. Gut für Sie! Interessierte Anleger laufen nicht enttäuscht ins Leere und Sie können jederzeit wieder dazu verdienen.</p>
                </div>
                <div class="grid_8 preffix_2 marg150">
                    <button type="button" class="btn_01 width100">Jetzt kostenfrei testen!</button>
                </div>
            </div>
        </div>
    </section>

    <section data-section="2">
        <div class="container-fluid">
            <div class="grid_10 preffix_1 marg300 width-tablet-wide-100">
                <h3 class="center">So <span class="line">einfach</span> geht es!</h3>
                <div class="grid_8 preffix_2 kreise">
                    <div class="kreis-3">
                        <div class="bg-schritt-1">
                        </div>
                    </div>
                    <div class="kreis-3">
                        <div class="bg-schritt-2">
                        </div>
                    </div>
                    <div class="kreis-3">
                        <div class="bg-schritt-3">
                        </div>
                    </div>
                </div>
                <div class="grid_8 preffix_2">
                    <div class="grid_4 center">
                        <p class="big160">1.</p>
                        <p class="big120">Kunden und Partner werben</p>
                    </div>
                    <div class="grid_4 center">
                        <p class="big160">2.</p>
                        <p class="big120">Registrieren und Account erstellen</p>
                    </div>
                    <div class="grid_4 center">
                        <p class="big160">3.</p>
                        <p class="big120">Provisionen abholen</p>
                    </div>
                </div>
                <div class="grid_8 preffix_2 marg150">
                    <button type="button" class="btn_01 width100">Jetzt kostenfrei testen!</button>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-basiccolor" data-section="3">
        <div class="container-fluid">
            <div class="grid_12 marg300">
                <h2 class="center">Sie können nur gewinnen – registrieren Sie sich jetzt!</h2>
                <div class="pad150 marg300">
                    <div class="round border big"><img src="img/icons/digitalunternehmer.svg" /> </div>
                    <h3 class="uppercase">Ganz einfach - wenig Aufwand</h3>
                    <p>Wir übernehmen für Sie die Dokumentation durch den transparenten, digitalen
                        Vermittlungsprozess. Ihren vermittelten Bestand und Ihre Provisionen finden Sie stets in
                        Ihrem persönlichen Dashboard. Die Provisionen werden monatlich
                        ausgezahlt.</p>
                </div>
                <div class="pad150">
                    <div class="round border big float-right"><img src="img/icons/zukunft.svg" /> </div>
                    <h3 class="uppercase">Zukunft sichern!</h3>
                    <p>Für Ihre Kunden sind Sie bereits Ansprechpartner mit innovativen Ideen und Experte für
                        alternative Investments und Vermögensanlagen. Als Moneywell-Partner können Sie Ihren
                        Kunden einen weiteren guten Tipp an die Hand geben – ganz einfach per Mausklick.</p>
                </div>
                <div class="pad150 marg300">
                    <div class="round border big"><img src="img/icons/zusatzverdienst.svg" /> </div>
                    <h3 class="uppercase">Ganz einfach - wenig Aufwand</h3>
                    <p>Mit 1 – 2 Stunden die Woche 300,- bis 500,- Euro pro Monat dazu verdienen  - probieren Sie es 3 Monate unverbindlich aus.</p>
                </div>
                <div class="pad150">
                    <div class="round border big float-right"><img src="img/icons/zulassungsfrei.svg" /> </div>
                    <h3 class="uppercase">Zukunft sichern!</h3>
                    <p>Um Ihr zusätzliches Einkommen zu erzielen, benötigen Sie keine Erlaubnis nach GewO, keine Haftpflichtversicherung oder ein Haftungsdach.</p>
                </div>
                <div class="grid_8 preffix_2 marg150 center">
                    <button type="button" class="btn_01">Jetzt kostenfrei testen!</button>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-grau" data-section="4">
        <div class="container-fluid bg-image">
            <div class="grid_12 marg300">
                <div class="grid_6 preffix_6">
                    <p class="big140">Spare Zeit, Geld und Stress mit dem
                        digitalen Partnerprogramm mit Real-
                        Time Kundengewinnung, automatisierter
                        Verwaltung und integriertem
                        Abrechnungssystem der Anleger.</p>
                </div>
            </div>
        </div>
    </section>

    <section data-section="4">
        <div class="container-fluid">
            <div class="grid_12 marg300">
                <ul class="img-list">
                    <li><img src="img/logos/Sachwert_Magazin_Moneywell.jpg"/> </li>
                    <li><img src="img/logos/finanzwelt_Moneywell.jpg"/> </li>
                    <li><img src="img/logos/MOIN-HH.jpg"/> </li>
                    <li><img src="img/logos/Creditreform_Moneywell.jpg"/> </li>
                    <li><img src="img/logos/CashOnline_Moneywell.jpg"/> </li>
                    <li><img src="img/logos/Wirtschafts_TV_Moneywell.jpg"/> </li>
                </ul>
            </div>
        </div>
    </section>

    <section data-section="5">
        <div class="container-fluid">
            <div class="grid_12 marg300 center">
                <h2 class="marg0-t">Unsere zufriedenen Kunden</h2>
                <div class="grid_7 ">
                    <img src="img/Bewertungen.jpg" alt="Kundenbewertungen von ausgezeichnet.org" />
                </div>
                <div class="grid_5">
                    <!--     Ausgezeichnet.org-Siegel: Begin -->
                    <!--div id="auorg-bg">
                        <a href="https://www.ausgezeichnet.org" target="_blank" title="Unabh&auml;ngige Bewertungen, Kundenbewertungen und G&uuml;tesiegel von     AUSGEZEICHNET.ORG" class="auorg-br">
                            <span style="font-size:8px;font-weight:normal;text-transform:uppercase;">    AUSGEZEICHNET.ORG</span>
                        </a>
                    </div>
                    <script type="text/javascript" src="//siegel.ausgezeichnet.org/widgets/js/5a048273dcf66542ea35d4a4/widget.js"></script>
                    <!--     Ausgezeichnet.org-Siegel: End -->
                </div>
            </div>
        </div>
    </section>

    <section class="bg-basiccolor" data-section="6">
        <div class="container-fluid">
            <div class="grid_10 preffix_1 marg300">
                <h2>Schnelle und unkomplizierte Zusammenarbeit in Minuten</h2>
                <p class="big120">Kein Papier, keine Selbstauskunft, kein Aufwand. Mit unserem mobilen und digitalen
                    Partnerprogramm kannst du deinen Account in unter 10 Minuten eröffnen und deine
                    digitale Kundengewinnung von überall aus erledigen. Du wirst durch Nachricht in
                    Echtzeit über jede aktuelle Transaktion informiert und bleibst immer über
                    Provisionsbewegungen auf dem Laufenden.</p>
                <div class="marg150 center">
                    <button type="button" class="btn_01">Jetzt kostenfreie Partner werden</button>
                </div>
            </div>

        </div>
    </section>

    <section  id="overlay">
        <div class="center">
            <h2>Empfehlung erhalten?</h2>
            <p class="big120">Bitte geben Sie hier den erhaltenen Partner-Code ein.</p>
            <form id="form-code">
                <input type="text" class="js-code" name="code1" id="code1" data-nr="1" placeholder="1" required>
                <input type="text" class="js-code" name="code2" id="code2" data-nr="2" placeholder="2" required>
                <input type="text" class="js-code" name="code3" id="code3" data-nr="3" placeholder="3" required>
                <input type="text" class="js-code" name="code4" id="code4" data-nr="4" placeholder="4" required>
                <input type="text" class="js-code" name="code5" id="code5" data-nr="5" placeholder="5" required>
                <p>Sobald Sie Ihren 5-stelligen Partner-Code eingetragen haben, wird Ihr Partner-Cockpit freigeschaltet.</p>
                <button type="submit" class="btn_0">Jetzt loslegen!</button>
            </form>
        </div>
    </section>


    <script type="text/javascript">
        let elemente = document.getElementsByClassName('js-code');
        if(undefined !== elemente || null !== elemente){
            for(let i = 0; i < elemente.length; i++){
                elemente[i].addEventListener('keyup', function (event) {
                    let aktNr = parseInt(this.getAttribute('data-nr')),
                        neueNr = aktNr + 1,
                        neueId = 'code'+neueNr,
                        neuesElement = document.getElementById(neueId);
                    if(null !== neuesElement){
                        neuesElement.focus();
                    }
                });
            }
        }

        let form = document.getElementById('form-code');
        if(undefined !== form && null !== form){
            form.onsubmit = function(ev){
                ev.preventDefault();
                let code = '';
                for(let i = 0; i < (this.elements.length - 1); i++){
                    code = code + this.elements.item(i).value;
                }
                let url = document.location.pathname.split('/')
                document.location.href = document.location.origin+'/'+url[1]+'/'+code;
            };
        }
        checkShortId();
        function checkShortId(){
            let short_id = document.getElementById('short_id').value;
            if(short_id === '' || short_id < 5){
                showOverlay();
            } else {
                loadDataUserId();
            }
        }

        function loadDataUserId(){

            let short_id = document.getElementById('short_id').value;

            xhr=new XMLHttpRequest();

            xhr.onreadystatechange=function()
            {
                if (xhr.readyState==4 && xhr.status==200)
                {
                    let response = xhr.responseText;
                    const obj = JSON.parse(response);
                    if(obj.data.length > 0){
                        loadDataUserLanding(obj.data[0].id);
                    } else {
                        showOverlay();
                    }
                }
            };

            xhr.open('POST', 'api', true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            data = {
                event: {
                    name: 'entity.load',
                    data: {
                        entity_name: 'user',
                        entity_filter: {short_id: short_id},
                        entity_fields: ['id']
                    }
                }
            };

            xhr.send("event="+JSON.stringify(data.event));
            return xhr;
        }

        function loadDataUserLanding(id) {
            xhr=new XMLHttpRequest();

            xhr.onreadystatechange=function()
            {
                if (xhr.readyState==4 && xhr.status==200)
                {
                    let response = xhr.responseText;
                    const obj = JSON.parse(response);
                    if(obj.data.length > 0){
                        setUserDataLanding(obj.data[0]);
                    }
                }
            };

            xhr.open('POST', 'api', true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            data = {
                event: {
                    name: 'entity.load',
                    data: {
                        entity_name: 'personal_landingpage',
                        entity_filter: {user_id: id}
                    }
                }
            };

            xhr.send("event="+JSON.stringify(data.event));
            return xhr;
        }

        function showOverlay(){
            document.getElementById('overlay').style.display = 'block';
        }

        function setUserDataLanding(data){
            if(data.video === '2'){
                document.getElementById('video-1').style.display = 'none';
                document.getElementById('video-2').style.display = 'block';
            }

            let sections = document.getElementsByTagName('section');
            for(let i = 0; i < sections.length; i++){
                let section = sections[i],
                    attr = section.getAttribute('data-section');
                if(null !== attr){
                    setSectionFromUserData(attr, data, section);
                }
            }
        }

        function setSectionFromUserData(nr, data, element){
            nr = parseInt(nr);
            switch (nr){
                case 1:
                    (data.div1 === '0')? element.style.display = 'none' : '';
                    break;
                case 2:
                    (data.div2 === '0')? element.style.display = 'none' : '';
                    break;
                case 3:
                    (data.div3 === '0')? element.style.display = 'none' : '';
                    break;
                case 4:
                    (data.div4 === '0')? element.style.display = 'none' : '';
                    break;
                case 5:
                    (data.div5 === '0')? element.style.display = 'none' : '';
                    break;
                case 6:
                    (data.div6 === '0')? element.style.display = 'none' : '';
                    break;
            }
        }



        (function()
        {
            var formanm = document.getElementById('anmeldeform');

            formanm.addEventListener('submit', function (event) {
                event.preventDefault();

                xhrAnm=new XMLHttpRequest();

                xhrAnm.onreadystatechange=function()
                {
                    if (xhrAnm.readyState==4 && xhrAnm.status==200)
                    {
                        //console.log('answer');
                        var response = xhrAnm.responseText;
                            resp_obj = JSON.parse(xhrAnm.response);
                            //console.log(response);

                        if(resp_obj.data != 'error') {
                            window.location.href = './email_erfolgreich.php?id='+resp_obj.data;
                            //alert(resp_obj.data);
                        }
                        else {
                            //alert(resp_obj.data);
                            $("#message").html('Daten vorhanden');
                            $("#message").fadeIn('slow', function () {
                                $("#message").delay(3000).fadeOut();

                            });
                        }

                    }
                };

                xhrAnm.open('POST', 'api', true);

                var form_data = new FormData();

                form_data.append('af_cmd', 'user.anmeldung');
                form_data.append('email_user', document.querySelectorAll('input[name="email"]')[0].value);
                form_data.append('id_partner', document.querySelectorAll('input[name="short_id"]')[0].value);

                xhrAnm.send(form_data);
            }, false);

        }())


        $('.js-mute').on('click', function (ev) {
            $(this).parent().find('video').prop('muted', false);
            $(this).parent().addClass('off');
            ev.preventDefault();
        });

    </script>

</main>
<footer>
    <p>Moneywell<sup>©</sup>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://www.moneywell-vertrieb.de/impressum/" target="_blank">Impressum</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://www.moneywell-vertrieb.de/datenschutz/" target="_blank">Datenschutz</a></p>
</footer>


<div id="cookie-notice" role="banner" class="cn-top wp-default" style="color: #fff; background-color: #a4a4a4;">
    <div class="cookie-notice-container">
            <span id="cn-notice-text">Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren und die Zugriffe auf unsere Website zu analysieren. Außerdem geben wir Informationen zu Ihrer Nutzung unserer Website an unsere Partner für soziale Medien, Werbung, Analysen und an die Moneywell Vertriebsgesellschaft GmbH weiter.
                <a href="https://www.moneywell-vertrieb.de/datenschutz/" style="color: #ff6a00;" target="_blank">Details.</a>
            </span>
        <a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie button wp-default" style="background: #ff6a00; color: white;">OK</a>
    </div>
</div>
</body>
</html>