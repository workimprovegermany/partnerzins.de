<?php
    $id = $_GET['id'];
    require_once("WincentApp.php");

    $app = WincentApp::get_instance();
    $ent = new Entity_mapper($app->db(), 'user_preaccount/1.0/config.xml');
    $fieldsSess = array(
        'session' => $id,
    );
    $data_user = $ent->find_by_fields($fieldsSess);
   // print_r($data_user);

    if($data_user[0]->field('bestaetigt') == 0) {
        $data_user[0]->set_field('bestaetigt',1);
        $ent->save($data_user[0]);
    }

    if($data_user[0]->field('aktiv') == 1) {
?>

    <!DOCTYPE html>
    <html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MONEYWELL © | Wie geht es weiter</title>
        <link rel="icon" type="image/x-icon" href="img/icon.ico">

        <link rel="stylesheet" href="app-foundation/css/dist/af.css">
        <link rel="stylesheet" href="css/dist/style.css">
        <script src="vendor/dist/vendor.min.js"></script>
        <script src="app-foundation/js/dist/af.min.js"></script>

        <link rel='stylesheet' id='cookie-notice-front-css'  href='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/css/front.min.css?ver=4.8.1' type='text/css' media='all' />
        <script type='text/javascript'>
            /* <![CDATA[ */
            var cnArgs = {"ajaxurl":"https:\/\/www.moneywell-vertrieb.de\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"no","onScrollOffset":"100","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":""};
            /* ]]> */
        </script>
        <script type='text/javascript' src='https://www.moneywell-vertrieb.de/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.42'></script>
    </head>
    <body>
    <main>
        <section class="bg-basiccolor">
            <div class="container-fluid">
                <div class="grid_12 center pad20">
                    <h2 class="uppercase">Moneywell Partnerprogramm</h2>
                </div>
            </div>
        </section>

        <section class="bg-grau">
            <div class="container-fluid">
                <div class="grid_10 preffix_1 marg300 farbverlauf">
                    <h2>Wie geht es weiter?</h2>
                    <div class="marg150 grid_12 border-solid">
                        <div class="viereckig">1.</div>
                        <p class="">Öffnen Sie Ihren Posteingang und halten Sie Ausschau nach der E-Mail: BITTE BESTÄTIGEN SIE IHRE REGISTRIERUNG.</p>
                    </div>

                    <div class="marg150 grid_12 border-solid">
                        <div class="viereckig">2.</div>
                        <p>Klicken Sie auf den Link in der E-Mail, um Ihre E-Mail-Adresse zu bestätigen.</p>
                    </div>

                    <div class="marg150 grid_12 border-solid">
                        <div class="viereckig">3.</div>
                        <p>Nach der Bestätigung der E-Mail bekommen Sie Ihren persönlichen Link zum Passwort eintragen.</p>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container-fluid">
                <div class="grid_10 preffix_1 marg300 farbverlauf">
                    <p>Mit freundlichen Grüßen</p>
                    <p>Ihr Monneywell</p>
                </div>
            </div>
        </section>


    </main>
    <footer>
        <p>Moneywell<sup>©</sup>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://www.moneywell-vertrieb.de/impressum/" target="_blank">Impressum</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
                    href="https://www.moneywell-vertrieb.de/datenschutz/" target="_blank">Datenschutz</a></p>
    </footer>


    <div id="cookie-notice" role="banner" class="cn-top wp-default" style="color: #fff; background-color: #a4a4a4;">
        <div class="cookie-notice-container">
            <span id="cn-notice-text">Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren und die Zugriffe auf unsere Website zu analysieren. Außerdem geben wir Informationen zu Ihrer Nutzung unserer Website an unsere Partner für soziale Medien, Werbung, Analysen und an die Moneywell Vertriebsgesellschaft GmbH weiter.
                <a href="https://www.moneywell-vertrieb.de/datenschutz/" style="color: #ff6a00;" target="_blank">Details.</a>
            </span>
            <a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie button wp-default" style="background: #ff6a00; color: white;">OK</a>
        </div>
    </div>
    </body>
    </html>
<?php
    }
    else {
        //header('Location: http://192.168.240.113/wincent.de');
        header('Location: http://wincent-online.de');
    }
?>

